/* Autounit test
 * Copyright (C) 2001-2002  Simon Janes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AUTOUNIT_C_TEST_H
#define AUTOUNIT_C_TEST_H

#include <stdarg.h>
#include <glib.h>

struct autounit_test_t_struct;

typedef gint (*autounit_test_fp_t)(struct autounit_test_t_struct *t);
typedef gint (*autounit_test_setup_fp_t)(struct autounit_test_t_struct *t);
typedef gint (*autounit_test_teardown_fp_t)(struct autounit_test_t_struct *t);
typedef gint (*autounit_test_set_status_fp)(struct autounit_test_t_struct *t,
					    char *msg);
/**
 * A function of this type should return
 * 0 for equal
 * 1 for ob1 greater than ob2
 * -1 for ob1 less than ob2
 */
typedef gint (*autounit_compare_func)(gpointer ob1, gpointer ob2);
typedef gchar* (*autounit_to_string_func)(gpointer ob1);

struct autounit_suite_t_struct {
  GString *suite_name;
  GString *suite_status;
  gboolean suite_run;
  gint suite_pct_complete;
  gdouble suite_seconds_elapsed;
  gulong suite_useconds_elapsed;
  gint total_tests;
  gint failed_tests;
    
  autounit_test_setup_fp_t setup_fp;
  autounit_test_teardown_fp_t teardown_fp;
  GSList *tests;
};
typedef struct autounit_suite_t_struct autounit_suite_t;

struct autounit_test_t_struct {
  autounit_suite_t *suite;

  GString *test_name;
  gboolean test_run;
  GString *test_status;
  gint failed_assertions;
  gint total_assertions;
  gdouble test_seconds_elapsed;
  gulong test_useconds_elapsed;
  autounit_test_fp_t test_fp;
  autounit_test_set_status_fp fail_fp;
  autounit_test_set_status_fp succeed_fp;
  gboolean do_fork;
  gpointer test_data;
  gint ref_count;
};
typedef struct autounit_test_t_struct autounit_test_t;

struct autounit_test_group_t_struct {
  char *name;
  autounit_test_fp_t test;
  gboolean enabled;
  gboolean forking;
};
typedef struct autounit_test_group_t_struct autounit_test_group_t;

struct autounit_stress_report_t_struct {
  gint     *round;
  gint     modulo;
};
typedef struct autounit_stress_report_t_struct autounit_stress_report_t;

/* Suite methods */
autounit_suite_t* au_new_suite(GString *suite_name,
                               autounit_test_setup_fp_t setup_fp,
                               autounit_test_teardown_fp_t teardown_fp);
void au_delete_suite(autounit_suite_t *t);

autounit_suite_t* au_add_test(autounit_suite_t *suite,
                              autounit_test_t *test);
autounit_suite_t* au_add_test_group(autounit_suite_t *suite,
                                    autounit_test_group_t *group);

gint au_run_suite(autounit_suite_t *suite);
gint au_run_stress_suite(autounit_suite_t *suite,
                         gint rounds,gint modulo);

/* Test methods */
autounit_test_t* au_new_test(GString *test_name,
			     autounit_test_fp_t test_fp);
void au_delete_test(autounit_test_t *t);

gint au_test_ref(autounit_test_t *t);
gint au_test_unref(autounit_test_t *t);

void au_remove_test(autounit_suite_t *tc, autounit_test_t *t);

void au_run_test(autounit_test_t *test);
void au_run_test_fork(autounit_test_t *test, autounit_stress_report_t *status);
void au_run_stress_test(autounit_test_t *test,
                        autounit_stress_report_t *report);

GString *au_test_serialize(autounit_test_t *test);
autounit_test_t *au_test_unserialize(GString *test_ser);

void au_test_set_fork_mode(autounit_test_t *t, gboolean mode);
gboolean au_test_get_fork_mode(autounit_test_t *t);

void au_test_append_msg(autounit_test_t *t, char *msg);

#define au_return_on_fail(func) \
    if(!func) { \
        return FALSE; \
    }

typedef enum
{
    AU_REL_GT,
    AU_REL_GT_EQ,
    AU_REL_LT,
    AU_REL_LT_EQ,
    AU_REL_EQUAL,
    AU_REL_NOTEQUAL
} AU_ASSERT_RELATION_TYPE;

gboolean au_assert_true(autounit_test_t *t, gboolean result, 
                        char *filename, int lineno, char *msg, ...);
gboolean au_assert_true_v(autounit_test_t *t, gboolean result, 
                          char *filename, int lineno, char *msg, va_list ap);

gboolean au_assert_str_int(autounit_test_t *t,
                           AU_ASSERT_RELATION_TYPE type,
                           char *str1, char *str2,
                           char *filename, int lineno,  char *msg, ...);
gboolean au_assert_str_int_v(autounit_test_t *t,
                             AU_ASSERT_RELATION_TYPE type,
                             char *str1, char *str2, 
                             char *filename, int lineno, char *msg,
                             va_list ap);

gboolean au_assert_guint64_int(autounit_test_t *t,
                               AU_ASSERT_RELATION_TYPE type,
                               guint64 in1, guint64 in2,
                               char *filename, int lineno, char *msg, ...);
gboolean au_assert_guint64_int_v(autounit_test_t *t,
                                 AU_ASSERT_RELATION_TYPE type,
                                 guint64 in1, guint64 in2,
                                 char *filename, int lineno, char *msg,
                                 va_list ap);

gboolean au_assert_gint64_int(autounit_test_t *t,
                              AU_ASSERT_RELATION_TYPE type,
                              gint64 in1, gint64 in2,
                              char *filename, int lineno, char *msg, ...);
gboolean au_assert_gint64_int_v(autounit_test_t *t,
                                AU_ASSERT_RELATION_TYPE type,
                                gint64 in1, gint64 in2,
                                char *filename, int lineno, char *msg,
                                va_list ap);

gboolean au_asserteq_char_int(autounit_test_t *t, char in1, char in2,
                              char *filename, int lineno, char *msg, ...);
gboolean au_asserteq_char_int_v(autounit_test_t *t, char in1, char in2,
                                char *filename, int lineno, char *msg,
                                va_list ap);

gboolean au_assert_obj_int(autounit_test_t *t,
                           AU_ASSERT_RELATION_TYPE type,
                           gpointer ob1, gpointer ob2,
                           autounit_compare_func func, 
                           autounit_to_string_func string_func1,
                           autounit_to_string_func string_func2,
                           char *filename, int lineno, char *msg, ...);
gboolean au_assert_obj_int_v(autounit_test_t *t,
                             AU_ASSERT_RELATION_TYPE type,
                             gpointer ob1, gpointer ob2,
                             autounit_compare_func compare_func,
                             autounit_to_string_func string_func1,
                             autounit_to_string_func string_func2,
                             char *filename, int lineno, char *msg,
                             va_list ap);


#if defined (__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define au_assert(t, expr, err_msg, ...) au_assert_true(t, expr, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_assert_fail(t, err_msg, ...) au_assert_true(t, 0, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_assert_succeed(t, err_msg, ...) au_assert_true(t, 1, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_asserteq_str(t, str1, str2, err_msg, ...) au_assert_str_int(t, AU_REL_EQUAL, str1, str2, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_assertrel_str(t, type, str1, str2, err_msg, ...) au_assert_str_int(t, type, str1, str2, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_asserteq_uint64(t, in1, in2, err_msg, ...) au_assert_guint64_int(t, AU_REL_EQUAL, in1, in2, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_assertrel_uint64(t, type, in1, in2, err_msg, ...) au_assert_guint64_int(t, type, in1, in2, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_asserteq_int(t, in1, in2, err_msg, ...) au_assert_gint64_int(t, AU_REL_EQUAL, in1, in2, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_assertrel_int(t, type, in1, in2, err_msg, ...) au_assert_gint64_int(t, type, in1, in2, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_asserteq_char(t, in1, in2, err_msg, ...) au_asserteq_char_int(t, in1, in2, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_asserteq_obj(t, ob1, ob2, compare_func, err_msg, ...) au_assert_obj_int(t, AU_REL_EQUAL, (gpointer)ob1, (gpointer)ob2, compare_func, 0, 0, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_assertrel_obj(t, type, ob1, ob2, compare_func, err_msg, ...) au_assert_obj_int(t, type, (gpointer)ob1, (gpointer)ob2, compare_func, 0, 0, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_assert_null(t, ob1, err_msg, ...) au_assert(t, ob1 == 0, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#define au_assert_not_null(t, ob1, err_msg, ...) au_assert(t, ob1 != 0, __FILE__, __LINE__, err_msg, __VA_ARGS__)

#elif defined (__GNUC__)
#define au_assert(t, expr, err_msg...) au_assert_true(t, expr, __FILE__, __LINE__, err_msg)

#define au_assert_fail(t, err_msg...) au_assert_true(t, 0, __FILE__, __LINE__, err_msg)

#define au_assert_succeed(t, err_msg...) au_assert_true(t, 1, __FILE__, __LINE__, err_msg)

#define au_asserteq_str(t, str1, str2, err_msg...) au_assert_str_int(t, AU_REL_EQUAL, str1, str2, __FILE__, __LINE__, err_msg)

#define au_assertrel_str(t, type, str1, str2, err_msg...) au_assert_str_int(t, type, str1, str2, __FILE__, __LINE__, err_msg)

#define au_asserteq_uint64(t, in1, in2, err_msg...) au_assert_guint64_int(t, AU_REL_EQUAL, in1, in2, __FILE__, __LINE__, err_msg)

#define au_assertrel_uint64(t, type, in1, in2, err_msg...) au_assert_guint64_int(t, type, in1, in2, __FILE__, __LINE__, err_msg)

#define au_asserteq_int(t, in1, in2, err_msg...) au_assert_gint64_int(t, AU_REL_EQUAL, in1, in2, __FILE__, __LINE__, err_msg)

#define au_assertrel_int(t, type, in1, in2, err_msg...) au_assert_gint64_int(t, type, in1, in2, __FILE__, __LINE__, err_msg)

#define au_asserteq_char(t, in1, in2, err_msg...) au_asserteq_char_int(t, in1, in2, __FILE__, __LINE__, err_msg)

#define au_asserteq_obj(t, ob1, ob2, compare_func, err_msg...) au_assert_obj_int(t, AU_REL_EQUAL, (gpointer)ob1, (gpointer)ob2, compare_func, 0, 0, __FILE__, __LINE__, err_msg)

#define au_assertrel_obj(t, type, ob1, ob2, compare_func, err_msg...) au_assert_obj_int(t, type, (gpointer)ob1, (gpointer)ob2, compare_func, 0, 0, __FILE__, __LINE__, err_msg)

#define au_assert_null(t, ob1, err_msg...) au_assert(t, ob1 == 0, __FILE__, __LINE__, err_msg)

#define au_assert_not_null(t, ob1, err_msg...) au_assert(t, ob1 != 0, __FILE__, __LINE__, err_msg)

#else

static gboolean
au_assert(autounit_test_t *t, gboolean expr, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_true_v(t, expr, __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_assert_fail(autounit_test_t *t, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_true_v(t, 0, __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_assert_succeed(autounit_test_t *t, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_true_v(t, 1, __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_assert_null(autounit_test_t *t, gpointer ob1, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_true_v(t, ob1 == 0, __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_assert_not_null(autounit_test_t *t, gpointer ob1, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_true_v(t, ob1 != 0, __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_asserteq_str(autounit_test_t *t, gchar *str1, gchar *str2, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_str_int_v(t, AU_REL_EQUAL, str1, str2, __FILE__, 0,
                              msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_assertrel_str(autounit_test_t *t, AU_ASSERT_RELATION_TYPE type,
                 gchar *str1, gchar *str2, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_str_int_v(t, type, str1, str2, __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_asserteq_uint64(autounit_test_t *t, guint64 in1, guint64 in2,
                   char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_guint64_int_v(t, AU_REL_EQUAL, in1, in2, __FILE__, 0,
                                    msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_assertrel_uint64(autounit_test_t *t, AU_ASSERT_RELATION_TYPE, type
                    guint64 in1, guint64 in2, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_guint64_int_v(t, type, in1, in2, __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_asserteq_int(autounit_test_t *t, gint64 in1, gint64 in2, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_gint64_int_v(t, in1, in2, __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_assertrel_int(autounit_test_t *t, AU_ASSERT_RELATION_TYPE type,
                  gint64 in1, gint64 in2, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_gint64_int_v(t, type, in1, in2, __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_asserteq_char(autounit_test_t *t, gchar in1, gchar in2, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_asserteq_char_int_v(t, in1, in2, __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_asserteq_obj(autounit_test_t *t, gpointer ob1, gpointer ob2,
                autounit_compare_func compare, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_obj_int_v(t, AU_REL_EQUAL, ob1, ob2, compare, 0, 0,
                              __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}

static gboolean
au_assertrel_obj(autounit_test_t *t, AU_ASSERT_RELATION_TYPE type,
                 gpointer ob1, gpointer ob2,
                 autounit_compare_func compare, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_obj_int_v(t, type, ob1, ob2, compare, 0, 0,
                              __FILE__, 0, msg, ap);
    va_end(ap);
    return ret;
}


#endif

#endif  /* AUTOUNIT_C_TEST_H */

