/* Autounit test
 * Copyright (C) 2001-2002  Simon Janes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <glib.h>

#include <autounit.h>

#include <au-netstring.h>

static int total_tests = 0;
static int failures = 0;
static GString *msgs = 0;

#define loc_assert(name, expr, msg) \
do { \
  if(!(expr)) { \
    char *errmsg = g_strdup_printf("%s:%d: %s: %s\n", __FILE__, __LINE__, \
                                   name, msg); \
    failures++; \
    msgs = g_string_append(msgs, errmsg); \
    g_free(errmsg); \
  } \
  total_tests++; \
} while(0)

typedef gboolean (*loc_test) (char *testname);

static gint 
test_sample_test(autounit_test_t *t)
{
    return TRUE;
}

static gboolean
test_au_create_and_destroy_test(char *testname)
{
  autounit_test_t *b_t;
  b_t = au_new_test(g_string_new("test_sample_test"), test_sample_test);
  
  loc_assert(testname, strcmp(b_t->test_name->str,"test_sample_test")==0,
             "test_name set incorrectly"); 
  loc_assert(testname, b_t->test_run==FALSE,
             "test_run not FALSE on virgin struct");
  loc_assert(testname, b_t->failed_assertions==0,
             "failed_assertions not 0 on virgin struct");
  loc_assert(testname,  b_t->test_seconds_elapsed==0.0,
             "test_seconds_elapsed not 0 on virgin struct");
  loc_assert(testname,  b_t->test_useconds_elapsed==0,
             "test_useconds_elapsed not 0 on virgin struct");
  loc_assert(testname,  b_t->suite == 0,
             "suite parent not NULL");
  loc_assert(testname,  b_t->do_fork == TRUE,
             "default do_fork set incorrectly"); 

  au_delete_test(b_t);
  return TRUE;
}

static gboolean
test_au_set_fork_mode(char *testname)
{
  autounit_test_t *b_t;
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  
  loc_assert(testname,  b_t->do_fork == TRUE,
             "default fork mode");
  au_test_set_fork_mode(b_t,FALSE);
  loc_assert(testname,  b_t->do_fork == FALSE,
             "changed fork mode (FALSE)");
  au_test_set_fork_mode(b_t,TRUE);
  loc_assert(testname,  b_t->do_fork == TRUE,
             "changed fork mode (TRUE)");
  
  au_delete_test(b_t);
  return TRUE;
}

static gboolean
test_au_default_status_called(char *testname)
{
  autounit_test_t *b_t;
  char *oddmessage;
  char *oddmessage2;
  char *oddmessage3;
  
  oddmessage = "SNTHEudA)R#$DT!THDEIT";
  oddmessage2 = "sntovwditne023rhdn";
  oddmessage3 = "vwo023n2tn3hd5yn";
  
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  au_assert(b_t, 0, oddmessage);
  loc_assert(testname, b_t->failed_assertions == 1,
             "failed_assertions should be 1");
  loc_assert(testname, b_t->total_assertions == 1,
             "total_assertions should be 1");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
             
  au_assert(b_t, 0, oddmessage2);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 2,
             "total_assertions should be 2");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage2) != 0,
             "oddmessage2 not found in test_status");

  au_assert(b_t, 1, oddmessage3);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 3,
             "total_assertions should be 3");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage2) != 0,
             "oddmessage2 not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage3) == 0,
             "oddmessage3 found in test_status");

  au_assert_fail(b_t, oddmessage);
  loc_assert(testname, b_t->failed_assertions == 3,
             "failed assertions should be 3");
  loc_assert(testname, b_t->total_assertions == 4,
             "total_assertions should be 4");

  au_assert_succeed(b_t, oddmessage);
  loc_assert(testname, b_t->failed_assertions == 3,
             "failed assertions should be 3");
  loc_assert(testname, b_t->total_assertions == 5,
             "total_assertions should be 5");
  
  au_delete_test(b_t);
  return TRUE;
}

static gboolean
test_au_str_assert_simple(char *testname)
{
  autounit_test_t *b_t;
  char *oddmessage;
  char *oddmessage2;
  char *oddmessage3;
  char *tmp;
  
  oddmessage = "SNTHEudA)R#$DT!THDEIT";
  oddmessage2 = "sntovwditne023rhdn";
  oddmessage3 = "vwo023n2tn3hd5yn";
  
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  au_asserteq_str(b_t, "foo", "bar", oddmessage);
  loc_assert(testname, b_t->failed_assertions == 1,
             "failed_assertions should be 1");
  loc_assert(testname, b_t->total_assertions == 1,
             "total_assertions should be 1");
  tmp = g_strdup_printf("%s: `%s' not == `%s'", oddmessage, "foo", "bar");
  loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
             "oddmessage not found in test_status");
  g_free(tmp);
             
  au_asserteq_str(b_t, "baz", "bar", oddmessage2);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 2,
             "total_assertions should be 2");
  tmp = g_strdup_printf("%s: `%s' not == `%s'", oddmessage, "foo", "bar");
  loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
             "oddmessage not found in test_status");
  g_free(tmp);
  tmp = g_strdup_printf("%s: `%s' not == `%s'", oddmessage2, "baz", "bar");
  loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
             "oddmessage2 not found in test_status");
  g_free(tmp);

  au_asserteq_str(b_t, "bar", "bar", oddmessage3);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 3,
             "total_assertions should be 3");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage2) != 0,
             "oddmessage2 not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage3) == 0,
             "oddmessage3 found in test_status");
  
  au_delete_test(b_t);
  return TRUE;
}

static gboolean
test_au_str_assert_complex(char *testname)
{
  autounit_test_t *b_t;
  char *oddmessage;
  char *oddmessage2;
  char *oddmessage3;
  
  oddmessage = "SNTHEudA)R#$DT!THDEIT";
  oddmessage2 = "sntovwditne023rhdn";
  oddmessage3 = "vwo023n2tn3hd5yn";
  
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  au_asserteq_str(b_t, 0, "bar", oddmessage);
  loc_assert(testname, b_t->failed_assertions == 1,
             "failed_assertions should be 1");
  loc_assert(testname, b_t->total_assertions == 1,
             "total_assertions should be 1");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
             
  au_asserteq_str(b_t, "baz", 0, oddmessage2);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 2,
             "total_assertions should be 2");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage2) != 0,
             "oddmessage2 not found in test_status");

  au_asserteq_str(b_t, 0, 0, oddmessage3);
  loc_assert(testname, b_t->failed_assertions == 3,
             "failed_assertions should be 3");
  loc_assert(testname, b_t->total_assertions == 3,
             "total_assertions should be 3");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage2) != 0,
             "oddmessage2 not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage3) != 0,
             "oddmessage3 found in test_status");
  
  au_delete_test(b_t);
  
  {
      int bad = 0;
      b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
      au_assertrel_str(b_t, AU_REL_EQUAL, "bar", "bar", 0);
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative eq: 1");
      au_assertrel_str(b_t, AU_REL_EQUAL, "bar", "foo", 0);
      bad++;
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative eq: 0");
      loc_assert(testname, strstr(b_t->test_status->str, "`bar' not == `foo'") != 0,
                 "not == not found in test_status");

      au_assertrel_str(b_t, AU_REL_NOTEQUAL, "bar", "foo", 0);
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative ne: 1");
      au_assertrel_str(b_t, AU_REL_NOTEQUAL, "bar", "bar", 0);
      bad++;
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative ne: 0");
      loc_assert(testname, strstr(b_t->test_status->str, "`bar' not != `bar'") != 0,
                 "not != not found in test_status");

      au_assertrel_str(b_t, AU_REL_GT, "foo", "bar", 0);
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative gt: 1");
      au_assertrel_str(b_t, AU_REL_GT, "bar", "foo", 0);
      bad++;
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative gt: 0");
      loc_assert(testname, strstr(b_t->test_status->str, "`bar' not > `foo'") != 0,
                 "not > not found in test_status");

      au_assertrel_str(b_t, AU_REL_GT_EQ, "foo", "bar", 0);
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative gteq(gt): 1");
      au_assertrel_str(b_t, AU_REL_GT_EQ, "bar", "bar", 0);
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative gteq(eq): 1");
      au_assertrel_str(b_t, AU_REL_GT_EQ, "bar", "foo", 0);
      bad++;
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative gteq: 0");
      loc_assert(testname, strstr(b_t->test_status->str, "`bar' not >= `foo'") != 0,
                 "not >= not found in test_status");

      au_assertrel_str(b_t, AU_REL_LT, "bar", "foo", 0);
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative lt: 1");
      au_assertrel_str(b_t, AU_REL_LT, "foo", "bar", 0);
      bad++;
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative lt: 0");
      loc_assert(testname, strstr(b_t->test_status->str, "`foo' not < `bar'") != 0,
                 "not < not found in test_status");

      au_assertrel_str(b_t, AU_REL_LT_EQ, "bar", "foo", 0);
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative lteq(lt): 1");
      au_assertrel_str(b_t, AU_REL_LT_EQ, "bar", "bar", 0);
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative lteq(eq): 1");
      au_assertrel_str(b_t, AU_REL_LT_EQ, "foo", "bar", 0);
      bad++;
      loc_assert(testname, b_t->failed_assertions == bad,
                 "relative lteq: 0");
      loc_assert(testname, strstr(b_t->test_status->str, "`foo' not <= `bar'") != 0,
                 "not <= not found in test_status");

      au_delete_test(b_t);
  }
  
  return TRUE;
}

static gboolean
test_au_gulong_assert_simple(char *testname)
{
  autounit_test_t *b_t;
  char *oddmessage;
  char *oddmessage2;
  char *oddmessage3;
  char *tmp;
  guint64 testval1;
  guint64 testval2;
  
  oddmessage = "SNTHEudA)R#$DT!THDEIT";
  oddmessage2 = "sntovwditne023rhdn";
  oddmessage3 = "vwo023n2tn3hd5yn";
  
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  testval1 = 9223372036854775808ull;
  testval2 = 9223372036854775807ull;
  au_asserteq_uint64(b_t, testval1, testval2, oddmessage);
  loc_assert(testname, b_t->failed_assertions == 1,
             "failed_assertions should be 1");
  loc_assert(testname, b_t->total_assertions == 1,
             "total_assertions should be 1");
  tmp = g_strdup_printf("%s: `%llu' not == `%llu'", oddmessage,
                        testval1, testval2);
  loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
             "oddmessage not found in test_status");
  g_free(tmp);

  testval1 = 832649;
  testval2 = 392;
  au_asserteq_uint64(b_t, testval1, testval2, oddmessage2);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 2,
             "total_assertions should be 2");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  tmp = g_strdup_printf("%s: `%llu' not == `%llu'", oddmessage2,
                        testval1, testval2);
  loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
             "oddmessage2 not found in test_status");
  g_free(tmp);

  au_asserteq_uint64(b_t, 9223372036854775804ull, 9223372036854775804ull,
                     oddmessage3);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 3,
             "total_assertions should be 3");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage2) != 0,
             "oddmessage2 not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage3) == 0,
             "oddmessage3 found in test_status");
  
  au_delete_test(b_t);
  return TRUE;
}

static gboolean
test_au_gulong_assert_complex(char *testname)
{
    autounit_test_t *b_t;
    int bad = 0;
    b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
    au_assertrel_uint64(b_t, AU_REL_EQUAL, 12315, 12315, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative eq: 1");
    au_assertrel_uint64(b_t, AU_REL_EQUAL, 12315, 223415, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative eq: 0");
    loc_assert(testname, strstr(b_t->test_status->str, "`12315' not == `223415'") != 0,
               "not <= not found in test_status");

    au_assertrel_uint64(b_t, AU_REL_NOTEQUAL, 12315, 223415, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative ne: 1");
    au_assertrel_uint64(b_t, AU_REL_NOTEQUAL, 12315, 12315, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative ne: 0");

    au_assertrel_uint64(b_t, AU_REL_GT, 223415, 12315, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative gt: 1");
    au_assertrel_uint64(b_t, AU_REL_GT, 12315, 223415, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative gt: 0");

    au_assertrel_uint64(b_t, AU_REL_GT_EQ, 223415, 12315, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative gteq(gt): 1");
    au_assertrel_uint64(b_t, AU_REL_GT_EQ, 12315, 12315, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative gteq(eq): 1");
    au_assertrel_uint64(b_t, AU_REL_GT_EQ, 12315, 223415, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative gteq: 0");

    au_assertrel_uint64(b_t, AU_REL_LT, 12315, 223415, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative lt: 1");
    au_assertrel_uint64(b_t, AU_REL_LT, 223415, 12315, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative lt: 0");

    au_assertrel_uint64(b_t, AU_REL_LT_EQ, 12315, 223415, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative lteq(lt): 1");
    au_assertrel_uint64(b_t, AU_REL_LT_EQ, 12315, 12315, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative lteq(eq): 1");
    au_assertrel_uint64(b_t, AU_REL_LT_EQ, 223415, 12315, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative lteq: 0");

    au_delete_test(b_t);

    return TRUE;
}

static gboolean
test_au_glong_assert_simple(char *testname)
{
  autounit_test_t *b_t;
  char *oddmessage;
  char *oddmessage2;
  char *oddmessage3;
  char *tmp;
  gint64 testval1;
  gint64 testval2;
  
  oddmessage = "SNTHEudA)R#$DT!THDEIT";
  oddmessage2 = "sntovwditne023rhdn";
  oddmessage3 = "vwo023n2tn3hd5yn";
  
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  testval1 = 23372036854775808ll;
  testval2 = 23372036854775807ll;
  au_asserteq_int(b_t, testval1, testval2, oddmessage);
  loc_assert(testname, b_t->failed_assertions == 1,
             "failed_assertions should be 1");
  loc_assert(testname, b_t->total_assertions == 1,
             "total_assertions should be 1");
  tmp = g_strdup_printf("%s: `%lld' not == `%lld'", oddmessage,
                        testval1, testval2);
  loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
             "oddmessage not found in test_status");
  g_free(tmp);

  testval1 = -832649;
  testval2 = -392;
  au_asserteq_int(b_t, testval1, testval2, oddmessage2);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 2,
             "total_assertions should be 2");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  tmp = g_strdup_printf("%s: `%lld' not == `%lld'", oddmessage2,
                        testval1, testval2);
  loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
             "oddmessage2 not found in test_status");
  g_free(tmp);

  au_asserteq_int(b_t, -3372036854775804ll, -3372036854775804ll,
                   oddmessage3);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 3,
             "total_assertions should be 3");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage2) != 0,
             "oddmessage2 not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage3) == 0,
             "oddmessage3 found in test_status");
  
  au_delete_test(b_t);
  return TRUE;
}

static gboolean
test_au_glong_assert_complex(char *testname)
{
    autounit_test_t *b_t;
    int bad = 0;
    b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
    au_assertrel_int(b_t, AU_REL_EQUAL, 12315, 12315, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative eq: 1");
    au_assertrel_int(b_t, AU_REL_EQUAL, 12315, 223415, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative eq: 0");

    au_assertrel_int(b_t, AU_REL_NOTEQUAL, 12315, 223415, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative ne: 1");
    au_assertrel_int(b_t, AU_REL_NOTEQUAL, 12315, 12315, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative ne: 0");

    au_assertrel_int(b_t, AU_REL_GT, 223415, 12315, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative gt: 1");
    au_assertrel_int(b_t, AU_REL_GT, 12315, 223415, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative gt: 0");

    au_assertrel_int(b_t, AU_REL_GT_EQ, 223415, 12315, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative gteq(gt): 1");
    au_assertrel_int(b_t, AU_REL_GT_EQ, 12315, 12315, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative gteq(eq): 1");
    au_assertrel_int(b_t, AU_REL_GT_EQ, 12315, 223415, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative gteq: 0");

    au_assertrel_int(b_t, AU_REL_LT, 12315, 223415, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative lt: 1");
    au_assertrel_int(b_t, AU_REL_LT, 223415, 12315, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative lt: 0");

    au_assertrel_int(b_t, AU_REL_LT_EQ, 12315, 223415, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative lteq(lt): 1");
    au_assertrel_int(b_t, AU_REL_LT_EQ, 12315, 12315, 0);
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative lteq(eq): 1");
    au_assertrel_int(b_t, AU_REL_LT_EQ, 223415, 12315, 0);
    bad++;
    loc_assert(testname, b_t->failed_assertions == bad,
               "relative lteq: 0");

    au_delete_test(b_t);

    return TRUE;
}

static gboolean
test_au_char_assert_simple(char *testname)
{
  autounit_test_t *b_t;
  char *oddmessage;
  char *oddmessage2;
  char *oddmessage3;
  char *tmp;
  
  oddmessage = "SNTHEudA)R#$DT!THDEIT";
  oddmessage2 = "sntovwditne023rhdn";
  oddmessage3 = "vwo023n2tn3hd5yn";
  
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  au_asserteq_char(b_t, 'c', 'd', oddmessage);
  loc_assert(testname, b_t->failed_assertions == 1,
             "failed_assertions should be 1");
  loc_assert(testname, b_t->total_assertions == 1,
             "total_assertions should be 1");
  tmp = g_strdup_printf("%s: `%s' not == `%s'", oddmessage, "c", "d");
  loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
             "oddmessage not found in test_status");
  g_free(tmp);
  
  au_asserteq_char(b_t, 'r', 'z', oddmessage2);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 2,
             "total_assertions should be 2");
  tmp = g_strdup_printf("%s: `%s' not == `%s'", oddmessage, "c", "d");
  loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
             "oddmessage not found in test_status");
  g_free(tmp);
  tmp = g_strdup_printf("%s: `%s' not == `%s'", oddmessage2, "r", "z");
  loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
             "oddmessage2 not found in test_status");
  g_free(tmp);

  au_asserteq_char(b_t, 'a', 'a', oddmessage3);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 3,
             "total_assertions should be 3");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage2) != 0,
             "oddmessage2 not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage3) == 0,
             "oddmessage3 found in test_status");
  
  au_delete_test(b_t);
  return TRUE;
}

static gboolean
test_au_var_arg_list(char *testname)
{
    autounit_test_t *b_t;
    char *form;
    int test_int;
    char *test_str;
    char *tmp;
    
    form = "%d %s FOOBAR";
    test_int = 1243122;
    test_str = "my test string";
    
    b_t = au_new_test(g_string_new("test_sample_test"), test_sample_test);
    au_assert (b_t, 0, form, test_int, test_str);
    loc_assert(testname, b_t->failed_assertions == 1,
               "failed_assertions should be 1");
    loc_assert(testname, b_t->total_assertions == 1,
               "total_assertions should be 1");
    tmp = g_strdup_printf(form, test_int, test_str);
    loc_assert(testname, strstr(b_t->test_status->str, tmp) != 0,
               "oddmessage not found in test_status");
    g_free(tmp);

    au_delete_test(b_t);
    return TRUE;
}
    
typedef struct
{
    char *name;
    int val;
    int foo;
} TestObjType1;

static gint
compare_testobjtype1(void *t1, void *t2)
{
    gint ret;
    TestObjType1 *ob1 = (TestObjType1*)t1;
    TestObjType1 *ob2 = (TestObjType1*)t2;
    
    ret = strcmp(ob1->name, ob2->name);
    if (ret != 0)
    {
        return ret;
    }
    if (ob1->val > ob2->val)
    {
        return 1;
    }
    else if (ob1->val < ob2->val)
    {
        return -1;
    }

    if (ob1->foo > ob2->foo)
    {
        return 1;
    }
    else if (ob1->foo < ob2->foo)
    {
        return -1;
    }

    return 0;
}

static gboolean
test_au_object_assert_simple(char *testname)
{
  autounit_test_t *b_t;

  TestObjType1 v1;
  TestObjType1 v2;
  TestObjType1 v3;

  char *oddmessage;
  char *oddmessage2;
  char *oddmessage3;
  
  v1.name = "foobar";
  v1.val = 10;
  v1.foo = 100;
  v2.name = "barfoo";
  v2.val = 20;
  v2.foo = 1298;
  v3.name = "foobar";
  v3.val = 10;
  v3.foo = 100;
  
  oddmessage = "SNTHEudA)R#$DT!THDEIT";
  oddmessage2 = "sntovwditne023rhdn";
  oddmessage3 = "vwo023n2tn3hd5yn";
  
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  au_asserteq_obj(b_t, &v1, &v2, compare_testobjtype1, oddmessage);
  loc_assert(testname, b_t->failed_assertions == 1,
             "failed_assertions should be 1");
  loc_assert(testname, b_t->total_assertions == 1,
             "total_assertions should be 1");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
             
  au_asserteq_obj(b_t, &v2, &v3, compare_testobjtype1, oddmessage2);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 2,
             "total_assertions should be 2");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage2) != 0,
             "oddmessage2 not found in test_status");

  au_asserteq_obj(b_t, &v1, &v3, compare_testobjtype1, oddmessage3);
  loc_assert(testname, b_t->failed_assertions == 2,
             "failed_assertions should be 2");
  loc_assert(testname, b_t->total_assertions == 3,
             "total_assertions should be 3");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage) != 0,
             "oddmessage not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage2) != 0,
             "oddmessage2 not found in test_status");
  loc_assert(testname, strstr(b_t->test_status->str, oddmessage3) == 0,
             "oddmessage3 found in test_status");
  
  au_delete_test(b_t);
  return TRUE;
}

static gboolean
test_au_object_assert_complex(char *testname)
{
  int bad = 0;

  autounit_test_t *b_t;

  TestObjType1 v1;
  TestObjType1 v2;
  TestObjType1 v3;

  v1.name = "foobar";
  v1.val = 10;
  v1.foo = 100;
  v2.name = "barfoo";
  v2.val = 20;
  v2.foo = 1298;
  v3.name = "barfoo";
  v3.val = 20;
  v3.foo = 1298;
  
  b_t = au_new_test(g_string_new("test_sample_test"), test_sample_test);
  au_assertrel_obj(b_t, AU_REL_EQUAL, &v2, &v3, compare_testobjtype1, 0);
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative eq: 1");
  au_assertrel_obj(b_t, AU_REL_EQUAL, &v2, &v1, compare_testobjtype1, 0);
  bad++;
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative eq: 0");

  au_assertrel_obj(b_t, AU_REL_NOTEQUAL, &v2, &v1, compare_testobjtype1, 0);
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative ne: 1");
  au_assertrel_obj(b_t, AU_REL_NOTEQUAL, &v2, &v3, compare_testobjtype1, 0);
  bad++;
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative ne: 0");

  au_assertrel_obj(b_t, AU_REL_GT, &v1, &v2, compare_testobjtype1, 0);
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative gt: 1");
  au_assertrel_obj(b_t, AU_REL_GT, &v2, &v1, compare_testobjtype1, 0);
  bad++;
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative gt: 0");

  au_assertrel_obj(b_t, AU_REL_GT_EQ, &v1, &v2, compare_testobjtype1, 0);
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative gteq(gt): 1");
  au_assertrel_obj(b_t, AU_REL_GT_EQ, &v2, &v3, compare_testobjtype1, 0);
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative gteq(eq): 1");
  au_assertrel_obj(b_t, AU_REL_GT_EQ, &v2, &v1, compare_testobjtype1, 0);
  bad++;
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative gteq: 0");

  au_assertrel_obj(b_t, AU_REL_LT, &v2, &v1, compare_testobjtype1, 0);
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative lt: 1");
  au_assertrel_obj(b_t, AU_REL_LT, &v1, &v2, compare_testobjtype1, 0);
  bad++;
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative lt: 0");

  au_assertrel_obj(b_t, AU_REL_LT_EQ, &v2, &v1, compare_testobjtype1, 0);
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative lteq(lt): 1");
  au_assertrel_obj(b_t, AU_REL_LT_EQ, &v2, &v3, compare_testobjtype1, 0);
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative lteq(eq): 1");
  au_assertrel_obj(b_t, AU_REL_LT_EQ, &v1, &v2, compare_testobjtype1, 0);
  bad++;
  loc_assert(testname, b_t->failed_assertions == bad,
             "relative lteq: 0");

  au_delete_test(b_t);
  return TRUE;
}

typedef struct 
{
    char *test_name;
    loc_test test_fp;
} loc_test_t;

static loc_test_t tests[] = {
  {"au_create_and_destroy_test", test_au_create_and_destroy_test},
  {"au_set_fork_mode", test_au_set_fork_mode},
  {"au_default_status_callback", test_au_default_status_called },
  {"au_str_assert_simple", test_au_str_assert_simple},
  {"au_str_assert_complex", test_au_str_assert_complex},
  {"au_gulong_assert_simple", test_au_gulong_assert_simple},
  {"au_gulong_assert_complex", test_au_gulong_assert_complex},
  {"au_glong_assert_simple", test_au_glong_assert_simple},
  {"au_glong_assert_complex", test_au_glong_assert_complex},
  {"au_char_assert_simple", test_au_char_assert_simple},
  {"au_object_assert_simple", test_au_object_assert_simple},
  {"au_object_assert_complex", test_au_object_assert_complex},
  {"au_var_arg_list", test_au_var_arg_list},
  {0, 0}
};

int
main(int argc, char *argv[])
{
    loc_test_t *testptr;
    gboolean succeeded;

    msgs = g_string_new("");
    
    succeeded = TRUE;
    for(testptr = tests; testptr->test_name != 0; testptr++)
    {
        if(testptr->test_fp(testptr->test_name) == FALSE) 
        {
            succeeded = FALSE;
        }
    }

    if (failures > 0) 
    {
        printf("%d of %d tests FAILED\n", failures, total_tests);
        printf(msgs->str);
        succeeded = FALSE;
    }
    
    exit(succeeded ? EXIT_SUCCESS : EXIT_FAILURE);
}

