/* Autounit test
 * Copyright (C) 2001-2002  Simon Janes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <autounit.h>
#include <glib.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

static int total_tests = 0;
static int failures = 0;
static GString *msgs = 0;

#define loc_assert(name, expr, msg) \
do { \
  if(!(expr)) { \
    char *errmsg = g_strdup_printf("%s:%d: %s: %s\n", __FILE__, __LINE__, \
                                   name, msg); \
    failures++; \
    msgs = g_string_append(msgs, errmsg); \
    g_free(errmsg); \
  } \
  total_tests++; \
} while(0)

typedef gboolean (*loc_test) (char *testname);

static gint
cus_setup_suite(autounit_test_t *t)
{
    return TRUE;
}

static gint
cus_teardown_suite(autounit_test_t *t)
{
    return TRUE;
}

static gint 
test_sample_test(autounit_test_t *t)
{
    return TRUE;
}

static gint
test_au_new_suite(char *testname)
{
  autounit_suite_t *x_tc;

  x_tc = au_new_suite(g_string_new("Mindbending Self Test"), 0, 0);

  loc_assert(testname, x_tc != 0, "au_new_suite returned NULL");
  loc_assert(testname, strcmp(x_tc->suite_name->str,"Mindbending Self Test") == 0,
	    "suite_name set incorrectly"); 
  loc_assert(testname, strcmp(x_tc->suite_status->str, "") == 0,
	    "suite_status not blank on virgin struct");
  loc_assert(testname, x_tc->suite_pct_complete == 0,
	    "suite_pct_complete not 0% on virgin struct");
  loc_assert(testname, x_tc->suite_seconds_elapsed == 0,
	    "suite_seconds_elapsed not 0 on virgin struct");
  loc_assert(testname, x_tc->suite_useconds_elapsed == 0,
	    "suite_useconds_elapsed not 0 on virgin struct");
  loc_assert(testname, x_tc->tests == NULL, "tests not NULL on virgin struct");
  loc_assert(testname, x_tc->setup_fp == NULL, "setup not NULL on virgin struct");
  loc_assert(testname, x_tc->teardown_fp == NULL,
	    "teardown not NULL on virgin struct");
  
  au_delete_suite(x_tc);

  x_tc = au_new_suite(g_string_new("Mindbending Self Test"), cus_setup_suite,
		      cus_teardown_suite);
    loc_assert(testname, x_tc != 0, "au_new_suite returned NULL");
  loc_assert(testname, strcmp(x_tc->suite_name->str,"Mindbending Self Test") == 0,
	    "suite_name set incorrectly"); 
  loc_assert(testname, strcmp(x_tc->suite_status->str, "") == 0,
	    "suite_status not blank on virgin struct");
  loc_assert(testname, x_tc->suite_pct_complete == 0,
	    "suite_pct_complete not 0% on virgin struct");
  loc_assert(testname, x_tc->suite_seconds_elapsed == 0,
	    "suite_seconds_elapsed not 0 on virgin struct");
  loc_assert(testname, x_tc->suite_useconds_elapsed == 0,
	    "suite_useconds_elapsed not 0 on virgin struct");
  loc_assert(testname, x_tc->tests == NULL, "tests not NULL on virgin struct");
  loc_assert(testname, x_tc->setup_fp == cus_setup_suite,
	    "setup not NULL on virgin struct");
  loc_assert(testname, x_tc->teardown_fp == cus_teardown_suite,
	    "teardown not NULL on virgin struct");

  au_delete_suite(x_tc);
  return TRUE;
}

static gint
test_au_add_test(char *testname)
{
  autounit_test_t *x_t;
  autounit_test_t *x_t2;
  autounit_suite_t *x_tc;
  x_tc = au_new_suite(g_string_new("Mindbending Self Test"), 0, 0);
  loc_assert(testname, x_tc != 0, "au_new_suite returned NULL");
  x_t = au_new_test(g_string_new("test_sample_test"), test_sample_test);
  loc_assert(testname, x_t != 0, "au_new_test returned NULL");
  
  loc_assert(testname, g_slist_length(x_tc->tests) == 0,
	    "suite should have empty list");
  au_add_test(x_tc,x_t);
  loc_assert(testname,g_slist_length(x_tc->tests)==1,
	    "different test list length than expected");
  x_t2 = au_new_test(g_string_new("test_sample_test2"),
		test_sample_test);
  au_add_test(x_tc,x_t2);
  loc_assert(testname,g_slist_length(x_tc->tests)==2,
	    "different test list length than expected");
  au_delete_suite(x_tc);
  return TRUE;
}

static gint
test_au_remove_test(char *testname)
{
  autounit_suite_t *b_tc;
  autounit_test_t     *b_t;
  b_tc = au_new_suite(g_string_new("Mindbending Self Test"),0,0);
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  au_add_test(b_tc,b_t);
  loc_assert(testname, g_slist_length(b_tc->tests)==1,
	    "length of tests list is not 1 after add");
  au_remove_test(b_tc,b_t);
  loc_assert(testname, g_slist_length(b_tc->tests)==0,
	    "length of tests list is not 1 after one deletion");
  au_remove_test(b_tc,b_t);
  loc_assert(testname, g_slist_length(b_tc->tests)==0,
	    "length of tests list is not 1 after one deletion");
  au_delete_suite(b_tc);
  return TRUE;
}

static gint
test_au_remove_ref_test(char *testname)
{
  autounit_suite_t *b_tc;
  autounit_test_t     *b_t;
  b_tc = au_new_suite(g_string_new("Mindbending Self Test"),0,0);
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  au_test_ref(b_t);
  au_add_test(b_tc,b_t);
  loc_assert(testname, g_slist_length(b_tc->tests)==1,
	    "length of tests list is not 1 after add");
  au_remove_test(b_tc,b_t);
  loc_assert(testname, g_slist_length(b_tc->tests)==0,
	    "length of tests list is not 1 after one deletion");
  au_remove_test(b_tc,b_t);
  loc_assert(testname, g_slist_length(b_tc->tests)==0,
	    "length of tests list is not 1 after one deletion");
  au_delete_test(b_t);
  au_delete_suite(b_tc);
  return TRUE;
}

typedef struct 
{
    char *test_name;
    loc_test test_fp;
} loc_test_t;

static loc_test_t tests[] = {
  {"au_create_test", test_au_new_suite},
  {"au_add_test", test_au_add_test},
  {"au_remove_test", test_au_remove_test},
  {"au_remove_ref_test", test_au_remove_ref_test},
  {0, 0}
};

int
main(int argc, char *argv[])
{
    loc_test_t *testptr;
    gboolean succeeded;

    msgs = g_string_new("");
    
    succeeded = TRUE;
    for(testptr = tests; testptr->test_name != 0; testptr++)
    {
        if(testptr->test_fp(testptr->test_name) == FALSE) 
        {
            succeeded = FALSE;
        }
    }

    if (failures > 0) 
    {
        printf("%d of %d tests FAILED\n", failures, total_tests);
        printf(msgs->str);
        succeeded = FALSE;
    }
    
    exit(succeeded ? EXIT_SUCCESS : EXIT_FAILURE);
}

