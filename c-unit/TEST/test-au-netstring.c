/* Autounit test
 * Copyright (C) 2001-2002  Simon Janes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <unistd.h>

#include <glib.h>

#include <autounit.h>
#include <au-netstring.h>

gint
test_au_gstring_to_netstring(autounit_test_t *t) 
{
  GString *ns; 
  ns = au_gstring_to_netstring(g_string_new("1234567890:::abc"));
  au_assert(t, ns->len==20,
	    "ns len doesn't match what we expect");
  au_assert(t, strcmp(ns->str, "16:1234567890:::abc,")==0,
	    "ns content doesn't match what we expect");
  g_string_free(ns,TRUE);
  return TRUE;
}

gint
test_au_netstring_to_gstring(autounit_test_t *t) 
{
  GString *ns; 
  GString *str; 
  ns = au_gstring_to_netstring(g_string_new("1234567890:::abc"));
  str = au_netstring_to_gstring(ns);
  au_assert(t, str->len==16,
	    "str len doesn't match what we expect");
  au_assert(t,	    strcmp(str->str, "1234567890:::abc")==0,
	    "str content doesn't match what we expect");
  g_string_free(ns,TRUE);
  g_string_free(str,TRUE);
  return TRUE;
}

gint
test_au_gboolean_to_netstring(autounit_test_t *t) 
{
  GString *ns; 
  gboolean b_TRUE = TRUE;
  gboolean b_FALSE = FALSE;

  ns = au_gboolean_to_netstring(b_TRUE);
  au_assert(t, ns->len==4,
	    "ns len doesn't match what we expect");
  au_assert(t,	    strcmp(ns->str, "1:1,")==0,
	    "ns content doesn't match what we expect");
  g_string_free(ns,TRUE);

  ns = au_gboolean_to_netstring(b_FALSE);
  au_assert(t, ns->len==4,
	    "ns len doesn't match what we expect");
  au_assert(t,	    strcmp(ns->str, "1:0,")==0,
	    "ns content doesn't match what we expect");
  g_string_free(ns,TRUE);
  return TRUE;
}

gint
test_au_netstring_to_gboolean(autounit_test_t *t) 
{
  gboolean b_TRUE = TRUE;
  gboolean b_FALSE = FALSE;
  GString *ns_FALSE; 
  GString *ns_TRUE; 
  gboolean v_TRUE;
  gboolean v_FALSE;

  ns_TRUE = au_gboolean_to_netstring(b_TRUE);
  ns_FALSE = au_gboolean_to_netstring(b_FALSE);
  v_TRUE = au_netstring_to_gboolean(ns_TRUE);
  au_assert(t,v_TRUE == TRUE, "gboolean not TRUE");
  v_FALSE = au_netstring_to_gboolean(ns_FALSE);
  au_assert(t,v_FALSE == FALSE, "gboolean not FALSE");

  g_string_free(ns_FALSE,TRUE);
  g_string_free(ns_TRUE,TRUE);
  return TRUE;
}

gint
test_au_netstring_to_gint(autounit_test_t *t) 
{
  gint fourtytwo = 42;
  GString *ns_fourtytwo; 
  gint v_42;

  ns_fourtytwo = au_gint_to_netstring(fourtytwo);
  v_42 = au_netstring_to_gint(ns_fourtytwo);
  au_assert(t,v_42 == 42,"gint not 42");

  g_string_free(ns_fourtytwo,TRUE);
  return TRUE;
}

gint
test_au_netstring_to_gulong(autounit_test_t *t) 
{
  gint fourtytwoM = 42000000;
  GString *ns_fourtytwoM; 
  gint v_42M;

  ns_fourtytwoM = au_gint_to_netstring(fourtytwoM);
  v_42M = au_netstring_to_gulong(ns_fourtytwoM);
  au_assert(t,v_42M == 42000000,"gulong not 42000000");

  g_string_free(ns_fourtytwoM,TRUE);
  return TRUE;
}

gint
test_au_netstring_to_gdouble(autounit_test_t *t)
{
  gdouble nnone = 99.1;
  GString *ns_nnone; 
  gdouble v_nnone;

  ns_nnone = au_gdouble_to_netstring(nnone);
  v_nnone = au_netstring_to_gdouble(ns_nnone);
  au_assert(t,v_nnone == 99.1,"double not 99.1");
  
  g_string_free(ns_nnone,TRUE);
  return TRUE;
}


gint
test_au_gint_to_netstring(autounit_test_t *t) 
{
  GString *ns; 
  gint b_42 = 42;
  gint b_n42 = -42;

  ns = au_gint_to_netstring(b_42);
  au_assert(t, ns->len==5,
	    "ns len doesn't match what we expect");
  au_assert(t, strcmp(ns->str, "2:42,")==0,
	    "ns content doesn't match what we expect");
  g_string_free(ns,TRUE);

  ns = au_gint_to_netstring(b_n42);
  au_assert(t, ns->len==6,
	    "ns len doesn't match what we expect");
  au_assert(t,	    strcmp(ns->str, "3:-42,")==0,
	    "ns content doesn't match what we expect");
  g_string_free(ns,TRUE);
  return TRUE;
}

gint
test_au_gdouble_to_netstring(autounit_test_t *t) 
{
  GString *ns; 
  gdouble b_pi = 4.5; /* Ok, it's a really bad approximation. */
  gdouble b_npi = -b_pi; /* Some bad arithmetic or lossage happens
			 **  here so I just fudged around it in the
			 **  "expected" results.  The point here is
			 **  testing whether the netstring is what
			 **  we're expecting, not the arithmetic.  I
			 **  just know this is going to blow-up
			 **  sometime when someone ports it somewhere
			 **  else.  It'll give someone some practise
			 **  using unit tests. ;) */

  ns = au_gdouble_to_netstring(b_pi);
  au_assert(t, ns->len==11,
	    "ns len doesn't match what we expect");
  au_assert(t,	    strcmp(ns->str, "8:4.500000,")==0,
	    "ns content doesn't match what we expect");
  g_string_free(ns,TRUE);

  ns = au_gint_to_netstring(b_npi);
  au_assert(t, ns->len==5,
	    "ns len doesn't match what we expect");
  au_assert(t,	    strcmp(ns->str, "2:-4,")==0,
	    "ns content doesn't match what we expect");
  g_string_free(ns,TRUE);
  return TRUE;
}

gint
test_au_gulong_to_netstring(autounit_test_t *t) 
{
  GString *ns; 
  gulong b_12345678 = 12345678;

  ns = au_gulong_to_netstring(b_12345678);
  au_assert(t, ns->len==11,
	    "ns len doesn't match what we expect");
  au_assert(t, strcmp(ns->str, "8:12345678,")==0,
	    "ns content doesn't match what we expect");
  g_string_free(ns,TRUE);
  return TRUE;
}

gint
test_au_read_netstring(autounit_test_t *t) 
{
  GString *hello_ns;
  GString *hello;
  FILE *test_fp;
  hello_ns = au_gstring_to_netstring(g_string_new("**"));
  /*
    printf("\nhello-nsstr = '%s'\n",hello_ns->str); 
    printf("\nhello-nsstr-len = %d\n",hello_ns->len); 
    */

  test_fp = fopen("tst.au_read_netstring","w");
  if (fwrite(hello_ns->str,hello_ns->len,sizeof(char),test_fp) < 0) {
    au_assert(t,0, "test failed to create data file");
    return FALSE;
  }
  fclose(test_fp);
  test_fp = fopen("tst.au_read_netstring","r");
  hello = au_read_netstring(fileno(test_fp));
  fclose(test_fp);
  unlink("tst.au_read_netstring");

  /* 
     printf("\nhello-str = '%s'\n",hello->str); 
     printf("\nhello-str-len = %d\n",hello->len); 
     */

  au_assert(t, strcmp(hello->str, "**")==0,
	    "hello content doesn't match what we expect");
  g_string_free(hello_ns,TRUE);
  g_string_free(hello,TRUE);
  return TRUE;
}

gint 
test_sample_test(autounit_test_t *t) 
{
    return TRUE;
}

gint
test_au_test_serialize_unserialize(autounit_test_t *t) 
{
  /* only needs to handle the "results" data, so we don't care about
  ** the suite pointer and test_fp pointer.
  **/
  GString *str_test;
  GString *read_test;
  autounit_test_t     *b_t;
  autounit_test_t *us_test;
  b_t = au_new_test(g_string_new("test_sample_test"),test_sample_test);
  str_test = au_test_serialize(b_t);
  au_assert(t,strcmp(str_test->str,
		     "46:16:test_sample_test,1:0,0:,1:0,8:0.000000,1:0,,")==0,
	    "serialized test is what we expect");
  /* When the serialized test is read from the child's pipe with au_read_netstream
   * it will have unwrapped the outer netstring encoding already, we'll simulate
   * this here with a call to au_netstring_to_gstring(). */
  read_test = au_netstring_to_gstring(str_test);
  us_test = au_test_unserialize(read_test);
  au_assert(t,us_test!=0,"us_test isn't allocated");
  au_assert(t,us_test->test_name!=0,"test_name isn't allocated");
  au_assert(t, strcmp(b_t->test_name->str, us_test->test_name->str)==0,
	    "test_name mismatch");
  au_assert(t,b_t->test_run == us_test->test_run,"test_run mismatch");
  au_assert(t,us_test->test_status!=0,"test_status isn't allocated");
  au_assert(t,strcmp(b_t->test_status->str,us_test->test_status->str)==0,
	    "test_status mismatch");
  au_assert(t,b_t->failed_assertions == us_test->failed_assertions,
	    "failed_assertions mismatch");
  au_assert(t,b_t->test_seconds_elapsed == us_test->test_seconds_elapsed,
	    "test_seconds_elapsed mismatch");
  au_assert(t,b_t->test_useconds_elapsed == us_test->test_useconds_elapsed,
	    "test_seconds_elapsed mismatch");
  au_delete_test(us_test); 
  g_string_free(read_test,TRUE);
  g_string_free(str_test,TRUE);
  au_delete_test(b_t);
  return TRUE;
}

gint
test_au_pop_netstring(autounit_test_t *t) {
  GString *test_ns;
  GString *test_str;
  GString *test_str2;
  GString *test_str_ns;
  GString *test_str2_ns;
  GString *test_pop;
  
  test_ns = g_string_new("");
  test_str = g_string_new("funk");
  test_str2 = g_string_new("groove");
  test_str_ns = au_gstring_to_netstring(test_str);
  test_str2_ns = au_gstring_to_netstring(test_str2);
  g_string_append(test_ns,test_str_ns->str);
  g_string_append(test_ns,test_str2_ns->str);

  test_pop = au_pop_netstring(test_ns);
  au_assert(t, strcmp(test_pop->str, test_str_ns->str)==0,
	    "test_pop is the first netstring in the 'stack' of netstrings");
  au_assert(t, strcmp(test_ns->str, test_str2_ns->str)==0,
	    "test_ns is the remainder");
  g_string_free(test_pop,TRUE);
  test_pop = au_pop_netstring(test_ns);
  au_assert(t, strcmp(test_pop->str, test_str2_ns->str)==0,
	    "test_pop is the last of the string");
  au_assert(t, test_ns->len == 0,
	    "test_pop should be empty now");
  g_string_free(test_ns,TRUE);
  g_string_free(test_str,TRUE);
  g_string_free(test_str2,TRUE);
  g_string_free(test_str_ns,TRUE);
  g_string_free(test_str2_ns,TRUE);
  g_string_free(test_pop,TRUE);
  return TRUE;
}

static autounit_test_group_t tests[] = {
  {"au_test_gstring_to_netstring", test_au_gstring_to_netstring, TRUE, TRUE},
  {"au_test_netstring_to_gstring", test_au_netstring_to_gstring, TRUE, TRUE},
  {"au_test_gboolean_to_netstring", test_au_gboolean_to_netstring, TRUE, TRUE},
  {"au_test_gboolean_to_netstring", test_au_gboolean_to_netstring, TRUE, TRUE},
  {"au_test_netstring_to_gboolean", test_au_netstring_to_gboolean, TRUE, TRUE},
  {"au_test_netstring_to_gint", test_au_netstring_to_gint, TRUE, TRUE},
  {"au_test_netstring_to_gulong", test_au_netstring_to_gulong, TRUE, TRUE},
  {"au_test_netstring_to_gdouble", test_au_netstring_to_gdouble, TRUE, TRUE},
  {"au_test_gint_to_netstring", test_au_gint_to_netstring, TRUE, TRUE},
  {"au_test_gdouble_to_netstring", test_au_gdouble_to_netstring, TRUE, TRUE},
  {"au_test_gulong_to_netstring", test_au_gulong_to_netstring, TRUE, TRUE},
  {"au_test_read_netstring", test_au_read_netstring, TRUE, TRUE},
  {"au_test_au_pop_netstring", test_au_pop_netstring, TRUE, TRUE},
  {"serialize/deserialize", test_au_test_serialize_unserialize, TRUE, TRUE},
  {0, 0, FALSE, FALSE}
};

int
main() {
  autounit_suite_t *c_unit_test_suite;
  autounit_suite_t *c_unit_stresstest_suite;
  gint result;

  c_unit_test_suite = au_new_suite(g_string_new("AuNetstring Tests"), 0, 0);
  au_add_test_group(c_unit_test_suite, tests);
  
  c_unit_stresstest_suite = au_new_suite(
      g_string_new("AuNetstring Stress Tests"), 0, 0);
  au_add_test_group(c_unit_stresstest_suite, tests);

  result = au_run_suite(c_unit_test_suite);

  if (!result) {
    result = au_run_stress_suite(c_unit_stresstest_suite,25,5);
  }

  au_delete_suite(c_unit_stresstest_suite);
  au_delete_suite(c_unit_test_suite);

  return result;
}
