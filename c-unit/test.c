/* Autounit test
 * Copyright (C) 2001-2002  Simon Janes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

#include <glib.h>

#include <au-netstring.h>
#include <autounit.h>
#include <autounit-private.h>

static gint au_default_fail_method(autounit_test_t *t, char *msg);
static gint au_default_succeed_method(autounit_test_t *t, char *msg);

autounit_test_t*
au_new_test(GString *testName,
	    autounit_test_fp_t test_fp)
{
  autounit_test_t *new_t = g_new(autounit_test_t, 1);
  new_t->test_name = g_string_new(testName->str);
  new_t->test_status = g_string_new("");
  new_t->test_run = FALSE;
  new_t->total_assertions = 0;
  new_t->failed_assertions = 0;
  new_t->test_seconds_elapsed = 0;
  new_t->test_useconds_elapsed = 0;
  new_t->test_fp = test_fp;
  new_t->do_fork = TRUE;
  new_t->suite = (autounit_suite_t*)0;
  new_t->fail_fp = au_default_fail_method;
  new_t->succeed_fp = au_default_succeed_method;
  new_t->test_data = (gpointer)0;
  new_t->ref_count = 0;
  return new_t;
}

/**
 * Free's memory used by test, please don't call this on anything
 * still linked into a suite, I'm trying to decide if it should call
 * remove for the user before it deletes.
 */
void
au_delete_test(autounit_test_t *t)
{
    if(t->ref_count <= 0)
    {
        g_string_free(t->test_name, TRUE);
        t->test_name = 0;
        g_string_free(t->test_status, TRUE);
        t->test_status = 0;
        g_free(t);
    }
}

void
au_test_set_fork_mode(autounit_test_t *t, gboolean mode)
{
  t->do_fork = mode;
}

gboolean
au_test_get_fork_mode(autounit_test_t *t)
{
    return t->do_fork;
}

gint
au_test_ref(autounit_test_t *t)
{
    return ++t->ref_count;
}

gint
au_test_unref(autounit_test_t *t)
{
    t->ref_count--;
    au_delete_test(t);
    return t->ref_count;
}

void
au_test_append_msg(autounit_test_t *t, char *msg)
{
    t->test_status = g_string_append(t->test_status, msg);
}

static gint
au_default_fail_method(autounit_test_t *t, char *msg)
{
    au_test_append_msg(t, msg);
    t->failed_assertions++;
    t->total_assertions++;
    return 1;
}

static gint
au_default_succeed_method(autounit_test_t *t, char *msg)
{
    t->total_assertions++;
    return 1;
}


/**
 * Convert a test structure into a stream of netstrings to pass back
 * via the pipe to the test case parent process.  We omit pointers.
 * This collection of netstrings itself then becomes one big
 * netstring.  In the future, it might be the case that Autounit
 * controls a farm of crosscompile machines... you never know. This
 * is the simplest thing I can think of that will satisfy being
 * reliable now and in the future.
 */
GString *
au_test_serialize(autounit_test_t *t)
{
  /**
   * the fields we care about...
   * GString *test_name;
   * gboolean test_run;
   * GString *test_status;
   * gint     failed_assertions;
   * gdouble  test_seconds_elapsed;
   * gulong   test_useconds_elapsed;
  */

  GString *str;
  GString *netstr;
  GString *tmp_test_name_ns;
  GString *tmp_test_run_ns;
  GString *tmp_test_status_ns;
  GString *tmp_failed_assertions_ns;
  GString *tmp_test_seconds_elapsed_ns;
  GString *tmp_test_useconds_elapsed_ns;
  
  tmp_test_name_ns = au_gstring_to_netstring(t->test_name);
  tmp_test_run_ns  = au_gboolean_to_netstring(t->test_run);
  tmp_test_status_ns = au_gstring_to_netstring(t->test_status);
  tmp_failed_assertions_ns = au_gint_to_netstring(t->failed_assertions);
  tmp_test_seconds_elapsed_ns = au_gdouble_to_netstring(
      t->test_seconds_elapsed);
  tmp_test_useconds_elapsed_ns = au_gulong_to_netstring(
      t->test_useconds_elapsed);

  str = g_string_new("");
  str = g_string_append(str,tmp_test_name_ns->str);
  str = g_string_append(str,tmp_test_run_ns->str);
  str = g_string_append(str,tmp_test_status_ns->str);
  str = g_string_append(str,tmp_failed_assertions_ns->str);
  str = g_string_append(str,tmp_test_seconds_elapsed_ns->str);
  str = g_string_append(str,tmp_test_useconds_elapsed_ns->str);
  netstr = au_gstring_to_netstring(str);

  g_string_free(str,TRUE);
  g_string_free(tmp_test_name_ns,TRUE);
  g_string_free(tmp_test_run_ns,TRUE);
  g_string_free(tmp_test_status_ns,TRUE);
  g_string_free(tmp_failed_assertions_ns,TRUE);
  g_string_free(tmp_test_seconds_elapsed_ns,TRUE);
  g_string_free(tmp_test_useconds_elapsed_ns,TRUE);

  return netstr;
}

autounit_test_t*
au_test_unserialize(GString *nst)
{
  /**
   * The fields we expect
   * string test_name
   * boolean test_run
   * string test_status
   * int failed_assertions
   * double test_seconds_elapsed
   * ulong test_useconds_elapsed
   */

  GString *test_name_ns;
  GString *test_run_ns;
  GString *test_status_ns;
  GString *failed_assertions_ns;
  GString *test_seconds_elapsed_ns;
  GString *test_useconds_elapsed_ns;
  GString *test_name;
  GString *test_status;
  autounit_test_t *us_test;
  GString *tmp_ns = g_string_new(nst->str);

  test_name_ns = au_pop_netstring(tmp_ns);
  test_run_ns = au_pop_netstring(tmp_ns);
  test_status_ns = au_pop_netstring(tmp_ns);
  failed_assertions_ns = au_pop_netstring(tmp_ns);
  test_seconds_elapsed_ns = au_pop_netstring(tmp_ns);
  test_useconds_elapsed_ns = au_pop_netstring(tmp_ns);
  
  test_name = au_netstring_to_gstring(test_name_ns);
  test_status = au_netstring_to_gstring(test_status_ns);
  
  us_test = au_new_test(test_name,0);
  us_test->test_run = au_netstring_to_gboolean(test_run_ns);
  g_string_append(us_test->test_status, test_status->str);
  us_test->failed_assertions = au_netstring_to_gint(failed_assertions_ns);
  us_test->test_seconds_elapsed = au_netstring_to_gdouble(
      test_seconds_elapsed_ns);
  us_test->test_useconds_elapsed = au_netstring_to_gulong(
      test_useconds_elapsed_ns);
  
  g_string_free(tmp_ns,TRUE);
  g_string_free(test_name,TRUE);
  g_string_free(test_status,TRUE);
  g_string_free(failed_assertions_ns,TRUE);
  g_string_free(test_seconds_elapsed_ns,TRUE);
  g_string_free(test_useconds_elapsed_ns,TRUE);
  
  return us_test;
}

void
au_run_test(autounit_test_t *t)
{
  /* Because there was basically no difference between au_run_test and
  ** au_run_stress_test, I decided to make the debugged forking-au_run_test
  ** a private function and the other two some wrappers. */
  autounit_stress_report_t stress_report;
  gint i = 1;

  au_log_start("au_run_test");
  stress_report.round = &i;
  stress_report.modulo = 1;

  au_run_test_fork(t, &stress_report);
  au_log_end("au_run_test");
}

void
au_run_stress_test(autounit_test_t *t,
		   autounit_stress_report_t *stress_report)
{
  au_log_start("au_run_stress_test");
  au_run_test_fork(t,stress_report);
  au_log_end("au_run_stress_test");
}

static void
au_run_test_child(autounit_test_t *t)
{
  GTimer *elapsed_timer;

  elapsed_timer = g_timer_new();
  {
    autounit_test_fp_t su_fn = (autounit_test_setup_fp_t) t->suite->setup_fp;
    autounit_test_fp_t fn = (autounit_test_fp_t) t->test_fp;
    autounit_test_fp_t td_fn = (autounit_test_teardown_fp_t) t->suite->teardown_fp;
    if (su_fn) su_fn(t);
    fn(t);
    if (td_fn) td_fn(t);
  }
  g_timer_stop(elapsed_timer);
  t->test_seconds_elapsed = g_timer_elapsed(elapsed_timer,
					    &t->test_useconds_elapsed);
  t->test_run = TRUE;
  g_timer_destroy(elapsed_timer); 
}

void
au_run_test_fork(autounit_test_t *t,
		 autounit_stress_report_t *status)
{
  /* start timer
  ** fork process
  ** in parent process:
  **   wait on child to return results and exit
  ** in new process:
  **   invoke setup_fp
  **   call test_fp
  **   call teardown_fp
  **   return results to parent process
  */
  au_log_start("au_run_test");

  if (t->do_fork) {
    int fork_result;
    int C2P_pipe[2];
    if (pipe(C2P_pipe) < 0) {
      au_assert(t, 0, _("pipe failed"));
      return;
    }

    switch(fork_result = fork()) {
    case -1:
      au_assert(t, 0, _("fork failed"));
      return;

    case 0:
      {
	/* Child */
	GString *child_test_result;
	au_run_test_child(t);
	child_test_result = au_test_serialize(t);
	close(C2P_pipe[0]);
	write(C2P_pipe[1],child_test_result->str,child_test_result->len);
	close(C2P_pipe[1]);
	au_log_end("au_run_test");
	exit(0);
      }

    default:
      {
	/* Parent */
	gint childstatus;
	close(C2P_pipe[1]);
	waitpid(fork_result, &childstatus, 0);
    
	if (WIFEXITED(childstatus)) {
	  autounit_test_t *tmp_t;
	  GString *parent_test_result = au_read_netstring(C2P_pipe[0]);
	  tmp_t = au_test_unserialize(parent_test_result);
    
	  /* Copy child's assertions to the parent's copy, we'll assume
	     that the test_name didn't change. */
	  t->failed_assertions += tmp_t->failed_assertions;
	  t->test_seconds_elapsed = tmp_t->test_seconds_elapsed;
	  t->test_useconds_elapsed = tmp_t->test_useconds_elapsed;
	  t->test_status = g_string_append(t->test_status,
                                           tmp_t->test_status->str);
          au_delete_test(tmp_t);
	} else {
	  if (WIFSIGNALED(childstatus)) {
	    au_assert(t, 0, _("test failed with signal"));
	  }
#ifdef WCOREDUMP          
	  if (WCOREDUMP(childstatus)) {
            t->failed_assertions++;
	    au_assert(t, 0, _("test failed with core dump"));
	  }
#endif          
	}
      }
    }
  }
  else {
    au_run_test_child(t);
  }
  
  if (t->failed_assertions > 0) {
    g_print(_("!"));
  } else {
    if ((*(status->round) % status->modulo) == 0) {
      g_print(_("."));
    }
  }
}


