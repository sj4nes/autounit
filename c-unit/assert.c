/* Autounit test
 * Copyright (C) 2001-2002  Simon Janes
 * Copyright (C) 2002 James Lewismoss
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/wait.h>

#include <glib.h>

#include <autounit.h>
#include <autounit-private.h>

gboolean
au_assert_true(autounit_test_t *t, gboolean result, 
               char *filename, int lineno, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_true_v(t, result, filename, lineno, msg, ap);
    va_end(ap);
    return ret;
}

gboolean
au_assert_true_v(autounit_test_t *t, gboolean result, 
                 char *filename, int lineno, char *msg, va_list ap)
{
    gboolean ret;
    autounit_test_set_status_fp fp;
    char *type;
    char *message;
    char *tmpmsg;
    
    if (!result)
    {
        ret = FALSE;
        type = _("FAIL");
        fp = t->fail_fp;
    } 
    else
    {
        ret = TRUE;
        type = _("SUCCEED");
        fp = t->succeed_fp;
    }

    tmpmsg = g_strdup_vprintf(msg, ap);

    message = g_strdup_printf("%s:%d:%s:%s\n", filename, lineno,
                              type, tmpmsg);
    fp(t, message);
    g_free(message);
    g_free(tmpmsg);
    return ret;
}

static gboolean
get_relative_value_guint64(AU_ASSERT_RELATION_TYPE type,
                           guint64 in1, guint64 in2)
{
    switch (type)
    {
    case AU_REL_GT:
        return (in1 > in2);
     case AU_REL_GT_EQ:
        return (in1 >= in2);
     case AU_REL_LT:
        return (in1 < in2);
     case AU_REL_LT_EQ:
        return (in1 <= in2);
     case AU_REL_EQUAL:
        return (in1 == in2);
     case AU_REL_NOTEQUAL:
        return (in1 != in2);
    default:
        return 0;
     }
}

static gboolean
get_relative_value_gint64(AU_ASSERT_RELATION_TYPE type,
                          gint64 in1, gint64 in2)
{
    switch (type)
    {
    case AU_REL_GT:
        return (in1 > in2);
    case AU_REL_GT_EQ:
        return (in1 >= in2);
    case AU_REL_LT:
        return (in1 < in2);
    case AU_REL_LT_EQ:
        return (in1 <= in2);
    case AU_REL_EQUAL:
        return (in1 == in2);
    case AU_REL_NOTEQUAL:
        return (in1 != in2);
    default:
        return 0;
    }
}

static const char *
get_relative_string(AU_ASSERT_RELATION_TYPE type)
{
    switch (type)
    {
    case AU_REL_GT:
        return ">";
    case AU_REL_GT_EQ:
        return ">=";
    case AU_REL_LT:
        return "<";
    case AU_REL_LT_EQ:
        return "<=";
    case AU_REL_EQUAL:
        return "==";
    case AU_REL_NOTEQUAL:
        return "!=";
    default:
        return "";
    }
}

gboolean
au_assert_str_int_v(autounit_test_t *t,
                    AU_ASSERT_RELATION_TYPE type,
                    char *str1, char *str2, 
                    char *filename, int lineno, char *msg, va_list ap)
{
    gboolean ret;
    char *message = g_strdup_printf(_("%s: `%s' not %s `%s'"), msg, str1,
                                    get_relative_string(type), str2);
    if (str1 != 0 && str2 != 0)
    {
        ret = au_assert_true_v(t,
                               get_relative_value_gint64(type,
                                                         strcmp(str1, str2),
                                                         0),
                               filename, lineno, message, ap);
    }
    else if ((str1 == 0 && str2 != 0) || (str1 != 0 && str2 == 0))
    {
        ret = au_assert_true_v(t, 0, filename, lineno, message, ap);
    }
    else /* FIXME: Surprising? */
    {
        ret = au_assert_true_v(t, 0, filename, lineno, message, ap);
    }
    g_free(message);
    return ret;
}


gboolean
au_assert_str_int(autounit_test_t *t,
                  AU_ASSERT_RELATION_TYPE type,
                  char *str1, char *str2, 
                  char *filename, int lineno, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_str_int_v(t, type, str1, str2, filename,
                              lineno, msg, ap);
    va_end(ap);
    return ret;
}

gboolean
au_assert_guint64_int_v(autounit_test_t *t,
                        AU_ASSERT_RELATION_TYPE type,
                        guint64 in1, guint64 in2,
                        char *filename, int lineno, char *msg, va_list ap)
{
    gboolean ret;
    char *message = g_strdup_printf(_("%s: `%llu' not %s `%llu'"), msg, in1,
                                    get_relative_string(type), in2);
    
    ret = au_assert_true_v(t, get_relative_value_guint64(type, in1, in2),
                           filename, lineno, message, ap);
    g_free(message);
    return ret;
}

gboolean
au_assert_guint64_int(autounit_test_t *t,
                      AU_ASSERT_RELATION_TYPE type,
                      guint64 in1, guint64 in2,
                      char *filename, int lineno, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_guint64_int_v(t, type, in1, in2, filename,
                                    lineno, msg, ap);
    va_end(ap);
    return ret;
}

gboolean
au_assert_gint64_int_v(autounit_test_t *t,
                       AU_ASSERT_RELATION_TYPE type,
                       gint64 in1, gint64 in2,
                       char *filename, int lineno, char *msg, va_list ap)
{
    gboolean ret;
    char *message = g_strdup_printf(_("%s: `%lld' not %s `%lld'"), msg, in1,
                                    get_relative_string(type), in2);
    ret = au_assert_true_v(t, get_relative_value_gint64(type, in1, in2),
                           filename, lineno, message, ap);
    g_free(message);
    return ret;
}

gboolean
au_assert_gint64_int(autounit_test_t *t,
                     AU_ASSERT_RELATION_TYPE type,
                     gint64 in1, gint64 in2,
                     char *filename, int lineno, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_gint64_int_v(t, type, in1, in2, filename, lineno, msg, ap);
    va_end(ap);
    return ret;
}

gboolean
au_asserteq_char_int_v(autounit_test_t *t, char in1, char in2,
                       char *filename, int lineno, char *msg, va_list ap)
{
    gboolean ret;
    char *message = g_strdup_printf(_("%s: `%c' not == `%c'"), msg, in1, in2);
    ret = au_assert_true_v(t, in1 == in2, filename, lineno, message, ap);
    g_free(message);
    return ret;
}

gboolean
au_asserteq_char_int(autounit_test_t *t, char in1, char in2,
                     char *filename, int lineno, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_asserteq_char_int_v(t, in1, in2, filename, lineno, msg, ap);
    va_end(ap);
    return ret;
}

gboolean
au_assert_obj_int_v(autounit_test_t *t,
                    AU_ASSERT_RELATION_TYPE type,
                    gpointer ob1, gpointer ob2,
                    autounit_compare_func compare_func,
                    autounit_to_string_func string_func1,
                    autounit_to_string_func string_func2,
                    char *filename, int lineno, char *msg, va_list ap)
{
    gboolean ret;
    char *ob1str;
    char *ob2str;
    char *message;

    ob1str = ((string_func1 != 0)
              ? string_func1(ob1)
              : g_strdup(_("(nostrfunc)")));
    ob2str = ((string_func2 != 0)
              ? string_func2(ob1)
              : g_strdup(_("(nostrfunc)")));
    message = g_strdup_printf("%s: `%s' not %s `%s'", msg, ob1str, get_relative_string(type), ob2str);
    
    if (ob1 != 0 && ob2 != 0)
    {
        if (compare_func != 0)
        {
            ret = au_assert_true_v(t,
                                   get_relative_value_gint64(
                                       type, compare_func(ob1, ob2), 0), 
                                   filename, lineno, message, ap);
        }
        else 
        {
            ret = au_assert_true_v(t, get_relative_value_gint64(
                                       type, (gint)ob1, (gint)ob2),
                                   filename, lineno, message, ap);
        }
    }
    else if ((ob1 == 0 && ob2 != 0) || (ob1 != 0 && ob2 == 0))
    {
        ret = au_assert_true_v(t, 0, filename, lineno, message, ap);
    }
    else /* FIXME: both null means they are not equal.  Surprising? */
    {
        ret = au_assert_true_v(t, 0, filename, lineno, message, ap);
    }
    g_free(message);
    g_free(ob1str);
    g_free(ob2str);
    return ret;
}

gboolean
au_assert_obj_int(autounit_test_t *t,
                  AU_ASSERT_RELATION_TYPE type,
                  gpointer ob1, gpointer ob2,
                  autounit_compare_func compare_func,
                  autounit_to_string_func string_func1,
                  autounit_to_string_func string_func2,
                  char *filename, int lineno, char *msg, ...)
{
    gboolean ret;
    va_list ap;
    va_start(ap, msg);
    ret = au_assert_obj_int_v(t, type, ob1, ob2, compare_func,
                              string_func1, string_func1,
                              filename, lineno, msg, ap);
    va_end(ap);
    return ret;
}

