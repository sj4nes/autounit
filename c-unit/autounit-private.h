#ifndef AUTOUNIT_PRIVATE_H
#define AUTOUNIT_PRIVATE_H

#include <config.h>
#include <glib.h>

#ifdef AUTOUNIT_C_UNIT_DEBUG
#define au_log(func, msg) \
    g_log(G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "%s %s", func, msg);
#define au_log_start(func) au_log(func, "START");
#define au_log_end(func) au_log(func, "END");
#else 
#define au_log(func, msg)
#define au_log_start(func)
#define au_log_end(func)
#endif

#include <libintl.h>
#define _(str) gettext(str)
#define N_(str) gettext_noop(str)

#endif /* AUTOUNIT_PRIVATE_H */
