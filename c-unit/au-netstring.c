/* Autounit test
 * Copyright (C) 2001-2002  Simon Janes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <autounit.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include <glib.h>
#include <au-netstring.h>
#include <autounit-private.h>

GString *
au_gstring_to_netstring(GString *s)
{
  /* Converts the contents of a GString into a djb-style netstring, why
  ** reinvent the wheel? 
  */
  GString *ns;
  ns = g_string_new("");
  g_string_sprintfa(ns,"%u:",s->len);
  ns = g_string_append(ns,s->str);
  ns = g_string_append(ns,",");
  return ns;
}

GString *
au_netstring_to_gstring(GString *ns)
{
  GString *str;
  size_t ns_len;
  char *tnbuf;
  char *tbuf;
  str = g_string_new("");
  sscanf(ns->str,"%d:",&ns_len);
  tnbuf = strchr(ns->str,(int)':')+1;
  tbuf = g_strndup(tnbuf,ns_len); /* strndup is a GNU extention, this is
				   just for prototyping. */
  str = g_string_append(str,tbuf);
  g_free(tbuf);
  return str;
}

GString *
au_gboolean_to_netstring(gboolean b)
{
  GString *ns;
  ns = g_string_new("");
  g_string_sprintfa(ns,"%u:",1);
  ns = g_string_append(ns,b==TRUE?"1":"0");
  ns = g_string_append(ns,",");
  return ns;
}

gboolean
au_netstring_to_gboolean(GString *ns)
{
  GString *str;
  size_t ns_len;
  char *tnbuf;
  gint tval;
  gboolean result;
  str = g_string_new("");
  sscanf(ns->str,"%d:",&ns_len);
  tnbuf = strchr(ns->str,(int)':')+1;
  sscanf(tnbuf,"%d",&tval);
  result = tval == 0 ? FALSE : TRUE;
  return result;
}

GString *
au_gint_to_netstring(gint gi)
{
  GString *gint_str;
  GString *ns;
  gint_str = g_string_new("");
  g_string_sprintf(gint_str,"%d",gi);
  ns = g_string_new("");
  g_string_sprintfa(ns,"%u:",gint_str->len);
  ns = g_string_append(ns,gint_str->str);
  ns = g_string_append(ns,",");
  g_string_free(gint_str,TRUE);
  return ns;
}

gint
au_netstring_to_gint(GString *ns)
{
  GString *str;
  size_t ns_len;
  char *tnbuf;
  gint tval;
  gint result;
  str = g_string_new("");
  sscanf(ns->str,"%d:",&ns_len);
  tnbuf = strchr(ns->str,(int)':')+1;
  sscanf(tnbuf,"%d",&tval);
  result = tval;
  g_string_free(str,TRUE);
  return result;
}

GString *
au_gulong_to_netstring(gulong gul)
{
  GString *gul_str;
  GString *ns;
  gul_str = g_string_new("");
  g_string_sprintf(gul_str,"%lu",gul);
  ns = g_string_new("");
  g_string_sprintfa(ns,"%u:",gul_str->len);
  ns = g_string_append(ns,gul_str->str);
  ns = g_string_append(ns,",");
  g_string_free(gul_str,TRUE);
  return ns;
}

gulong
au_netstring_to_gulong(GString *ns)
{
  GString *str;
  size_t ns_len;
  char *tnbuf;
  gulong tval;
  gulong result;
  str = g_string_new("");
  sscanf(ns->str,"%d:",&ns_len);
  tnbuf = strchr(ns->str,(int)':')+1;
  sscanf(tnbuf,"%lu",&tval);
  result = tval;
  g_string_free(str,TRUE);
  return result;
}

GString *
au_gdouble_to_netstring(gdouble gd)
{
  GString *gdouble_str;
  GString *ns;
  gdouble_str = g_string_new("");
  g_string_sprintf(gdouble_str,"%f",gd);
  ns = g_string_new("");
  g_string_sprintfa(ns,"%u:",gdouble_str->len);
  ns = g_string_append(ns,gdouble_str->str);
  ns = g_string_append(ns,",");
  g_string_free(gdouble_str,TRUE);
  return ns;
}

gdouble
au_netstring_to_gdouble(GString *ns)
{
  GString *str,*tmpstr;
  size_t ns_len;
  gdouble tval;
  gdouble result;
  str = g_string_new("");
  sscanf(ns->str,"%d:",&ns_len);
  tmpstr = g_string_new(strchr(ns->str,(int)':')+1);
  g_string_truncate(tmpstr,tmpstr->len-1);
  sscanf(tmpstr->str,"%lf",&tval);
  result = tval;
  g_string_free(str,TRUE);
  g_string_free(tmpstr,TRUE);
  return result;
}

GString *
au_read_netstring(int fd)
{
  /* Read a netstring from a file descriptor into a GString
     returns the contents of the netstring, not the netstring "wrapper".

     djb's sample code: (assumes you're reading from stdin, is here for
     reference)

      if (scanf("%9lu",&len) < 1) barf(); >999999999 bytes is bad
      if (getchar() != ':') barf();
      buf = malloc(len + 1);       malloc(0) is not portable
      if (!buf) barf();
      if (fread(buf,1,len,stdin) < len) barf();
      if (getchar() != ',') barf();
   */
  FILE *pipestream;
  GString *tns;
  gulong len;
  int read;
  pipestream = fdopen(fd,"r");
  if (!pipestream) {
    fprintf(stderr,_("fatal: %s\n"),strerror(errno));
    abort();
  }
  if (fscanf(pipestream,"%9lu",&len) < 1) {
    return 0;
  }
  if (fgetc(pipestream) != ':') {
    return 0;
  }
  tns = g_string_sized_new(len+1);
  read = 0;
  do
  {
      read += fread(tns->str + read, 1, len, pipestream);
  } while (read < len && !feof(pipestream));
  
  if (read < len) {
    g_string_free(tns,TRUE);
    return 0;
  }
  if (fgetc(pipestream) != ',') {
    g_string_free(tns,TRUE);
    return 0;
  }
  fclose(pipestream);
  tns->str[len] = 0;
  tns->len = len;
  return tns;
}

GString *
au_pop_netstring(GString *ns)
{
  /* Returns the netstring at the front of the GString, and changes
   * the input GString.  This API may change.  I'm not proud of this code.
   */
  GString *tns;
  gulong str_len;
  gint digit_len;
  char *digits;
  char *popped;
  GString *token_tmp;  

  token_tmp = g_string_new(ns->str);

  digits = strtok(token_tmp->str,":");

  if (!digits) {
    g_string_free(token_tmp,TRUE);
    return 0;
  }
  digit_len = strlen(digits);
  str_len = strtoul(digits,0,10);
  if (str_len > 9999999) {
    g_string_free(token_tmp,TRUE);
    return 0;
  }

  /* length ':' string ',' \0 */
  tns = g_string_sized_new(digit_len+1+str_len+1+1);
  popped = g_strndup(ns->str,digit_len+1+str_len+1);
  g_string_append(tns,popped);
  g_string_erase(ns,0,digit_len+1+str_len+1);
  free(popped);
  return tns;
}
