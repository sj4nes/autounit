/* Autounit test
 * Copyright (C) 2001-2002  Simon Janes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AU_NETSTRING_H
#define AU_NETSTRING_H

#include <glib.h>

/* Utility functions used internally by Autounit */
GString *au_gstring_to_netstring(GString *s);
GString *au_netstring_to_gstring(GString *ns);
GString *au_gboolean_to_netstring(gboolean b);
gboolean au_netstring_to_gboolean(GString *ns);
gint au_netstring_to_gint(GString *ns);
gdouble au_netstring_to_gdouble(GString *ns);
gulong au_netstring_to_gulong(GString *ns);
GString *au_gint_to_netstring(gint gi);
GString *au_gdouble_to_netstring(gdouble gd);
GString *au_gulong_to_netstring(gulong gul);
GString *au_read_netstring(int fd);
GString *au_pop_netstring(GString *ns);

#endif /* AU_NETSTRING_H */
