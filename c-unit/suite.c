/* Autounit test
 * Copyright (C) 2001-2002  Simon Janes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

#include <glib.h>

#include <au-netstring.h>
#include <autounit.h>
#include <autounit-private.h>

autounit_suite_t*
au_new_suite(GString *suiteName,
             autounit_test_setup_fp_t setup_fp,
             autounit_test_teardown_fp_t teardown_fp)
{
  autounit_suite_t *new_tc;
  au_log_start("au_new_suite");
  new_tc = g_new(autounit_suite_t,1);
  new_tc->suite_name = g_string_new(suiteName->str);
  new_tc->suite_status = g_string_new("");
  new_tc->suite_run = FALSE;
  new_tc->suite_pct_complete = 0;
  new_tc->suite_seconds_elapsed = 0;
  new_tc->suite_useconds_elapsed = 0;
  new_tc->setup_fp = (gpointer) setup_fp;
  new_tc->teardown_fp = (gpointer) teardown_fp;
  new_tc->tests = NULL;
  au_log_end("au_new_suite");
  return new_tc;
}

void
au_delete_suite(autounit_suite_t *tc)
{
  /* Free's memory used by test, please don't call this on anything
     still linked into a suite, I'm trying to decide if it should
     call remove for the user before it deletes. */
  g_string_free(tc->suite_name, TRUE);
  g_string_free(tc->suite_status, TRUE);
  while (tc->tests != NULL)
  {
      autounit_test_t *data;
      data = (autounit_test_t*)g_slist_nth(tc->tests, 0)->data;
      tc->tests = g_slist_remove(tc->tests, data);
      au_test_unref(data);
  }
  g_free(tc);
}

autounit_suite_t*
au_add_test(autounit_suite_t* tc, autounit_test_t *t)
{
  au_log_start("au_add_suite");
  t->suite = tc;
  tc->tests = g_slist_append(tc->tests,t);
  au_test_ref(t);
  tc->total_tests++;
  au_log_end("au_add_suite");
  return tc;
}

autounit_suite_t*
au_add_test_group(autounit_suite_t *tc, autounit_test_group_t *tg)
{
    int test_no;
    for(test_no = 0; tg[test_no].name != 0; test_no++)
    {
        if (tg[test_no].enabled == TRUE) {
            autounit_test_t *tmp_test;
            tmp_test = au_new_test(g_string_new(tg[test_no].name),
                                   tg[test_no].test);
            au_test_set_fork_mode(tmp_test, tg[test_no].forking);
            au_add_test(tc, tmp_test);
        } else {
            /* Warn user/programmer that a test has been disabled. */
            fprintf(stderr, _("?! '%s' test disabled\n"), tg[test_no].name);
        }
    }
    return tc;
}

void
au_remove_test(autounit_suite_t *tc, autounit_test_t *t)
{
  /* Remove first instance of test from suite if it exists. */
  if (tc->tests != NULL) {
    tc->tests = g_slist_remove(tc->tests,t);
    tc->total_tests--;
    au_test_unref(t);
  }
}

gboolean 
au_test_failed_check(autounit_suite_t *tc)
{
  gboolean failed_assertions_found;
  GSList* tmp;
  autounit_test_t *test;

  au_log_start("au_test_failed_check");
  failed_assertions_found = FALSE;
  tmp = tc->tests;

  while (tmp != NULL)
    {
      test = (autounit_test_t*) tmp->data;
      if (test->failed_assertions > 0) {
	failed_assertions_found = TRUE;
      }
     tmp = g_slist_next(tmp);
   }  
  au_log_end("au_test_failed_check");
  return failed_assertions_found;
}

static void
au_suite_gather_totals(autounit_suite_t *tc)
{
  /**
   * Count up elapsed time from all tests in suite and update
   * summary.
   */
  GSList* tmp;
  autounit_test_t *test;

  au_log_start("au_suite_gather_totals");
  tmp = tc->tests;
  while (tmp != NULL)
    {
      test = (autounit_test_t*) tmp->data;
      tc->suite_seconds_elapsed += test->test_seconds_elapsed;
      tc->suite_useconds_elapsed += test->test_useconds_elapsed;
      if (test->failed_assertions > 0)
      {
          tc->failed_tests++;
      }
      tmp = g_slist_next(tmp);
   }  
  au_log_end("au_suite_gather_totals");
}

static void
au_suite_reset_totals(autounit_suite_t *tc)
{
  /* Zero elapsed time for all tests in suite and zero
  ** summary elapsed time.
  */
  GSList* tmp;
  autounit_test_t *test;

  au_log_start("au_suite_reset_totals");
  tmp = tc->tests;
  while (tmp != NULL)
    {
      test = (autounit_test_t*) tmp->data;
      test->test_seconds_elapsed = 0; 
      test->test_useconds_elapsed = 0;
      tmp = g_slist_next(tmp);
   }  
  tc->suite_seconds_elapsed = 0;
  tc->suite_useconds_elapsed = 0;
  tc->failed_tests = 0;
  au_log_end("au_suite_reset_totals");
}

void
au_suite_fail_report(autounit_suite_t *tc)
{
  GSList* tmp;
  gint test_num;
  autounit_test_t* test;

  au_log_start("au_suite_fail_report");
  tmp = tc->tests;
  test_num = 0;
  g_print(_("FAIL %d of %d %s %f s (%ld us) total elapsed time\n"),
          tc->failed_tests, tc->total_tests,
          tc->suite_name->str,
	  tc->suite_seconds_elapsed, tc->suite_useconds_elapsed);
  while (tmp != NULL)
   {
     test = (autounit_test_t*)tmp->data;
     if (strlen(test->test_status->str)) {
       g_print(_("%s: elapsed time %f s (%ld us):failed assertions (%d of %d):\n"),
               test->test_name->str,
	       test->test_seconds_elapsed,
	       test->test_useconds_elapsed,
               test->failed_assertions, test->total_assertions);
       g_print("%s",test->test_status->str);
     } 
     tmp = g_slist_next(tmp);
   }
  au_log_end("au_suite_fail_report");
}

void
au_suite_ok_report(autounit_suite_t *tc)
{
  au_log_start("au_suite_ok_report");
  g_print(_("OK %d succeeded %f s (%ld us) total elapsed time\n"),
          tc->total_tests,
	  tc->suite_seconds_elapsed, tc->suite_useconds_elapsed);
  au_log_end("au_suite_fail_report");
}

gint
au_run_suite(autounit_suite_t *tc)
{
  gint result = 0;
  au_log_start("au_run_suite");
  au_suite_reset_totals(tc);
  g_print("%s\n", tc->suite_name->str);
  g_slist_foreach(tc->tests, (GFunc)au_run_test, 0);
  g_print("\n");

  au_suite_gather_totals(tc);
  if (au_test_failed_check(tc) == TRUE) {
    au_suite_fail_report(tc);
    result = 1;
  } else {
    au_suite_ok_report(tc);
  }

  au_log_end("au_run_suite");
  return result;
}

gint
au_run_stress_suite(autounit_suite_t *tc,
                    gint iterations,
                    gint status_modulo)
{
  gint result = 0;
  gint i;
  autounit_stress_report_t stress_report;

  au_log_start("au_run_stress_suite");
  stress_report.round = &i;
  stress_report.modulo = status_modulo;

  g_print(_("%s (%d iterations)\n"),tc->suite_name->str,iterations);
  au_suite_reset_totals(tc);
  for (i= 0; i < iterations; i++) {
    if ((*stress_report.round % stress_report.modulo) == 0)
      g_print("(%d)",i+1);

    g_slist_foreach(tc->tests ,(GFunc)au_run_stress_test, &stress_report);
    au_suite_gather_totals(tc);
    if (au_test_failed_check(tc) == TRUE) {
      au_suite_fail_report(tc);
      result = 1;
      /* Abort stress testing on first error */
      return result;
    }
    if (result) {
      break;
    }
  }
  g_print("\n");
  if (!result) {
    au_suite_ok_report(tc);
  } else { 
    au_suite_fail_report(tc);
  }
  au_log_end("au_run_stress_suite");
  return result;
}


