# GNU Autounit: Unit Testing Frameworks for Autoconf

Simon Janes <sj4nes@protonmail.com>
James LewisMoss <dres@debian.org>

GNU Autounit's goal is to provide a common unit testing framework for
application developers who use GNU Autoconf already in their projects but
do not currently use a unit testing framework.

This release of GNU Autounit (0.15.2) has support for unit testing in C
('c-unit') and adds support for Guile ('guile-unit') written by 
James LewisMoss.

	$ ./configure
	$ make check

