;;; Autounit test
;;; Copyright (C) 2002 James Lewismoss
 
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
 
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
;;; Library General Public License for more details.
 
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

(define-module (autounit test)
  :use-module (ice-9 syncase)
  :use-module (autounit assert))

(export au:test-make
        au:test-run
        au:test-tag
        au:is-test?
        au:test-get-name)

(define au:test-tag 'au:test)

(define (au:test-make name tests . args)
  (let ((setup #f)
        (teardown #f))
    (if (not (null? args))
        (if (not (null? (car args)))
            (begin
              (set! setup (car args))
              (if (not (null? (cdr args)))
                  (set! teardown (cadr args))))))
    (vector au:test-tag
            name
            tests
            setup
            teardown)))

(define (au:is-test? test)
  (and (vector? test)
       (eq? (vector-ref test 0) au:test-tag)))

(define (au:test-get-name test)
  (if (au:is-test? test)
      (vector-ref test 1)
      (throw au:autounit-misuse)))

(define (au:test-run suite test)
  (if (au:is-test? test)
      (letrec ((data (make-hash-table 13))
               (run-one-test
                (lambda (one-test)
                  (if (vector-ref test 3) ((vector-ref test 3) data))
                  (one-test suite data)
                  (if (vector-ref test 4) ((vector-ref test 4) data)))))
        (for-each run-one-test (vector-ref test 2)))
      (throw au:autounit-misuse)))


