;;; Autounit test
;;; Copyright (C) 2002 James Lewismoss
 
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
 
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
;;; Library General Public License for more details.
 
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

(define-module (autounit suite)
  :use-module (autounit backend)
  :use-module (autounit test))

(export
 au:suite-make
 au:suite-run
 au:suite-add-test 
 au:suite-run?
 au:suite-reset!
 au:is-suite?
 au:suite-do-exit
 au:suite-successful?
 au:suite-get-name
 au:suite-get-total
 au:suite-get-failures
 au:suite-get-messages
 au:suite-append-message
 au:suite-declare
 au:get-default-suite)

(define au:suite-tag 'au:suite)

(define tag-position 0)
(define name-position 1)
(define tests-position 2)
(define failures-position 3)
(define total-position 4)
(define messages-position 5)
(define has-run-position 6)

(define (au:suite-make name)
  (vector au:suite-tag
          name
          '() ;; tests
          0   ;; failures
          0   ;; tests run
          '() ;; messages
          #f  ;; has-run?
          ))

(define (au:suite-reset! suite)
  (vector-set! suite failures-position 0)
  (vector-set! suite total-position 0)
  (vector-set! suite messages-position '())
  (vector-set! suite has-run-position #f))

(define (au:is-suite? suite)
  (and (vector? suite)
       (eq? (vector-ref suite tag-position) au:suite-tag)))

(define (au:suite-get-name suite)
  (if (au:is-suite? suite)
      (vector-ref suite name-position)
      (throw au:autounit-misuse)))

(define (au:suite-run suite)
  (if (au:is-suite? suite)
      (let ((stack-str #f))
        (letrec ((run-one-test
                  (lambda (test)
                    (catch
                     #t
                     (lambda ()
                       (lazy-catch #t
                                   (lambda ()
                                     (au:test-run suite test)
                                     (au:suite-add-success suite test))
                                   (lambda (key . args)
                                     (let* ((stack (make-stack #t #f))
                                            (bt (call-with-output-string
                                                 (lambda (sp)
                                                   (display-backtrace
                                                    stack sp)))))
                                       (set! stack-str bt)
                                       (apply throw key args)))))
                     (lambda (key . args)
                       (case key
                         ((au:autounit-misuse)
                          (throw key args))
                         (else
                          (au:suite-add-failure suite test key
                                                args stack-str))))))))
          (vector-set! suite has-run-position #t)
          (for-each run-one-test (vector-ref suite tests-position))))
      (throw au:autounit-misuse)))

(define (suite-up-total suite)
  (vector-set! suite total-position
               (1+ (vector-ref suite total-position))))
(define (suite-up-failures suite)
  (vector-set! suite failures-position
               (1+ (vector-ref suite failures-position))))

(define (suite-make-exception-msg test exception args bt)
  (format "FAILURE: Test ~A failed with exception \"~A\" and args ~A\nException:\n~A"
          test exception args bt))

(define (au:suite-add-success suite test)
  (suite-up-total suite))

(define (au:suite-add-failure suite test exception args bt)
  (suite-up-total suite)
  (suite-up-failures suite)
  (au:suite-message-append suite #f
                           (suite-make-exception-msg test exception args bt)))

(define (au:suite-run? suite)
  (if (au:is-suite? suite)
      (vector-ref suite has-run-position)
      (throw au:autounit-misuse)))

(define (au:suite-successful? suite)
  (if (au:is-suite? suite)
      (if (au:suite-run? suite)
          (= 0 (au:suite-get-failures suite))
          (throw au:autounit-misuse))
      (throw au:autounit-misuse)))

(define (au:suite-get-total suite)
  (if (au:is-suite? suite)
      (vector-ref suite total-position)
      (throw au:autounit-misuse)))

(define (au:suite-get-failures suite)
  (if (au:is-suite? suite)
      (vector-ref suite failures-position)
      (throw au:autounit-misuse)))

(define (au:suite-get-messages suite)
  (if (au:is-suite? suite)
      (vector-ref suite messages-position)
      (throw au:autounit-misuse)))

(define (au:suite-message-append suite succ msg)
  (if (au:is-suite? suite)
      (vector-set! suite messages-position
                   (append (au:suite-get-messages suite)
                           (list (cons succ msg))))
      (throw au:autounit-misuse)))

(define (au:suite-add-test suite test)
  (if (au:is-suite? suite)
      (vector-set! suite tests-position
                   (append (vector-ref suite tests-position)
                           (list test)))
      (throw au:autounit-misuse)))

(define (au:suite-declare suite succ msg)
  (if (au:is-suite? suite)
      (begin
        (au:suite-message-append suite succ msg)
        (if (not succ)
            (throw au:failure-exception)))
      (throw au:autounit-misuse)))

(define (au:suite-do-exit suite)
  (if (au:suite-successful? suite)
      (exit 0)
      (letrec ((msg-printer
                (lambda (msg)
                  (if (not (car msg))
                      (begin
                        (display (cdr msg))
                        (newline))))))
        (for-each msg-printer (au:suite-get-messages suite))
        (exit 1))))

;;; Here's a default suite
(define au:default-suite  (au:suite-make "Autounit Default Suite"))

(define (au:get-default-suite) au:default-suite)
