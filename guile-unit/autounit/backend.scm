;;; Autounit test
;;; Copyright (C) 2002 James Lewismoss
 
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
 
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
;;; Library General Public License for more details.
 
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

(define-module (autounit backend)
  :use-module (autounit suite))

(export au:declare
        au:failure-exception
        au:autounit-misuse)

(define au:failure-exception 'au:failure)

(define au:autounit-misuse 'au:misuse)

(define (au:declare ob succ msg)
  (cond 
   ((au:is-suite? ob) (au:suite-declare ob succ msg))
   ((eq? ob #t) (au:suite-declare (au:get-default-suite) succ msg))
   ((eq? ob #f) (if (not succ) (throw au:failure-exception)))
   ((procedure? ob) (ob succ msg))
   (#t (throw au:autounit-misuse))))

