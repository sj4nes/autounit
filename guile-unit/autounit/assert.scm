;;; Autounit test
;;; Copyright (C) 2002 James Lewismoss
 
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
 
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
;;; Library General Public License for more details.
 
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

(define-module (autounit assert)
  :use-module (ice-9 syncase)
  :use-module (ice-9 slib)
  :use-module (ice-9 format)
  :use-module (autounit backend))

(export
 au:assert-equal
 au:assert-eq
 au:assert-true
 au:assert-false
 au:assert-success
 au:assert-failure
 au:assert-empty
 au:assert-not-empty
 au:generate-assert-msg
 ;; from (autounit backend)
 au:declare
 au:failure-exception)

(define (au:generate-assert-msg succ msg cond1 cond2)
  (let ((start (if succ "SUCCESS" "FAILURE"))
        (mid (if succ "equals" "does not equal")))
    (format "~A: \"~A\" :: ~%\"~A\" ~A ~%\"~A\"~%" start msg cond1 mid cond2)))

(define-syntax au:assert-failure
  (syntax-rules ()
                ((_ ob) (au:declare ob #f #f))
                ((_ ob msg) (au:declare ob #f msg))))

(define-syntax au:assert-success
  (syntax-rules ()
                ((_ ob) (au:declare ob #t #f))
                ((_ ob msg) (au:declare ob #t msg))))

(define-syntax au:assert-true
  (syntax-rules ()
                ((_ ob cond) (if cond
                                 (au:assert-success ob)
                                 (au:assert-failure ob)))
                ((_ ob cond msg) (if cond
                                     (au:assert-success ob msg)
                                     (au:assert-failure ob msg)))))

(define-syntax au:assert-false
  (syntax-rules ()
                ((_ ob cond) (if (not cond)
                                 (au:assert-success ob)
                                 (au:assert-failure ob)))
                ((_ ob cond msg) (if (not cond)
                                     (au:assert-success ob msg)
                                     (au:assert-failure ob msg)))))

(define-syntax au:assert-empty
  (syntax-rules ()
                ((_ ob obj) (if (null? obj)
                                (au:assert-success ob)
                                (au:assert-failure ob)))
                ((_ ob obj msg) (if (null? obj)
                                    (au:assert-success ob msg)
                                    (au:assert-failure ob msg)))))

(define-syntax au:assert-not-empty
  (syntax-rules ()
                ((_ ob obj) (if (not (null? obj))
                                (au:assert-success ob)
                                (au:assert-failure ob)))
                ((_ ob obj msg) (if (not (null? obj))
                                    (au:assert-success ob msg)
                                    (au:assert-failure ob msg)))))

(define-syntax au:assert-equal
  (syntax-rules ()
                ((_ ob cond1 cond2)
                 (if (equal? cond1 cond2)
                     (au:assert-success
                      ob (au:generate-assert-msg #t "" `cond1 `cond2))
                     (au:assert-failure
                      ob (au:generate-assert-msg #f "" `cond1 `cond2))))
                ((_ ob cond1 cond2 msg)
                 (if (equal? cond1 cond2)
                     (au:assert-success
                      ob (au:generate-assert-msg #t msg `cond1 `cond2))
                     (au:assert-failure
                      ob (au:generate-assert-msg #f msg `cond1 `cond2))))))

(define-syntax au:assert-eq
  (syntax-rules ()
                ((_ ob cond1 cond2)
                 (if (eq? cond1 cond2)
                     (au:assert-success
                      ob (au:generate-assert-msg #t "" `cond1 `cond2))
                     (au:assert-failure
                      ob (au:generate-assert-msg #f "" `cond1 `cond2))))
                ((_ ob cond1 cond2 msg)
                 (if (eq? cond1 cond2)
                     (au:assert-success
                      ob (au:generate-assert-msg #t msg `cond1 `cond2))
                     (au:assert-failure
                      ob (au:generate-assert-msg #f msg `cond1 `cond2))))))
