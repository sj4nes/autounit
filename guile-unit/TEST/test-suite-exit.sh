#! /bin/sh

EXIT_VALUE=0

./suite-exit0-test.scm
if test "$?" != "0" ; then
  echo "exit0 suite didn't return 0"
  EXIT_VALUE=1
fi

# Hide the "FAILURE" output, because that's confusing. We want only to
# detect the failure from the exit value.
./suite-exit1-test.scm > /dev/null 2>/dev/null
if test "$?" = "0" ; then
  echo "exit1 suite returned 0"
  EXIT_VALUE=1
fi

exit $EXIT_VALUE
