@node   Webliography, Function Index, C-Unit, Top
@chapter Webliography
@comment $Id: au-webliography.texi,v 1.2 2001/03/08 06:35:56 spj Exp $
@comment $Log: au-webliography.texi,v $
@comment Revision 1.2  2001/03/08 06:35:56  spj
@comment *** empty log message ***
@comment
@comment Revision 1.1  2001/03/02 04:51:07  spj
@comment c-unit-suite moved from test to c-unit dir.; c-unit texinfo documentation renamed / re-outlined for future language support
@comment

@itemize @bullet
@item
The GNU Autounit Home Page @url{http://recursism.com/projects/autounit/}
@item
Freshmeat Discussions about GNU Autounit @url{http://www.freshmeat.net/projects/autounit/}
@item 
Extreme Programming Wiki @url{http://c2.com/cgi/wiki?ExtremeProgramming}
@item
Aegis Software Configuration Management System @url{http://www.canb.auug.org.au/~millerp/aegis/}
@item
Test Suites and the Linux Kernel by Michael D. Crawford @url{http://linuxquality.sunsite.dk/articles/testsuites/}
@item
Linux Test Project @url{http://oss.sgi.com/projects/ltp/}


@end itemize
